# Route 53

---

# Summary

- TTL
- CNAME vs Alias
- Health Checks
- Routing Policies
  + Simple
  + Weighted
  + Latency
  + Failover
  + Geolocation
  + Multi Value
- 3<sup>rd</sup> party domains integration

---

## Overview

- Managed DNS
- Common records:
  + A
  + AAAA: hostname to IPv6
  + CNAME: hostname to hostname
  + Alias: hostname to AWS resource
- supports public & private domains 
- Advanced Features:
  + load balancing
  + Health checks
  + routing policy
- 0.50$ per month per hosted zone

---

## DNS Record TLL (Time to Live)

- tells client how long to hold onto DNS response
- clients have to wait for TTL to expire to see any changes to modified DNS records 
- High TTL: Less traffic, possibly outdated records
- low TTL: more traffic, records outdate for less time, easy to change
- **MANDATORY FOR EACH RECORD**

---

## CNAME vs Alias 

- AWS Resources expose AWS hostname, but want a friendly URL
- CNAME: 
  + points hostname to other hostname
  + **ONLY FOR NON ROOT DOMAIN** (has to be host, not root domain)
- Alias
  + Points hostname to AWS resources
  + **WORKS FOR ROOT DOMAIN & NON-ROOT DOMAIN**
  + free
  + Native health check 

---

## Routing Policies

- Simple
  + Map hostname to another hostname
  + use when need to redirect to single resources
  + can't attach health checks to simple routing policy 
  + **if multiple values returned, random value chosen _by client_**
- Weighted 
  + control % of rqsts that go to specific endpoint
  + helpful to test 1% of traffic on new app version 
  + helpful to split traffic betw/2 regions 
  + supports health checks
- Latency 
  + redirect to server w/least latency closest to client 
  + helpful when latency of users is priority
  + **latency evaluated in terms of user to designated AWS region** 
- Failover
  + health check required
  + if health check fails, route will failover to secondary instance 
  + only supports 1 primary & 1 secondary
- Geo-Location 
  + Different than latency, **based on user location, regardless of latency**
  + **BEST PRACTICE**: have default policy in case no location match
- Multi Value
  + when routing traffic to multiple resources 
  + associate Route 53 health checks w/Records
  + up to 8 healthy records returned for each Multi Value query
  + **NOT SUBSTITUE FOR HAVING ELB**

---

## Health Checks 

- if X health checks failed => unhealthy (**DEFAULT**: 3)
- after X health checks passed => healthy (**DEFAULT**: 3)
- **DEFAULT HEALTH CHECK INTERVAL**: 30s, can be 10s for more money
- **15 HEALTH CHECKERS WILL CHECK ENDPOINT HEALTH**
- on average, one request every 2 seconds
- supports HTTP/HTTPS, TCP health checks (no SSL verification)
- can integrate health check w/CloudWatch
- can be linked to Route53 DNS queries

---

## Domain Registrars

- *domain registrar*: org that manages reservation of domain names
- Route53 is registrar 
- using 3<sup>rd</sup> party registrar, can still use Route53
  + created hosted zone in Route 53
  + update NS records on 3<sup>rd</sup> party website to use Route53 **name servers**

