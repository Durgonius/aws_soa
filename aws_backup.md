# AWS Backup

- fully managed backup solution
- centrally managed
- supported services:
  + FSx
  + EFS
  + DynamoDB
  + EBS
  + RDS
  + Aurora
  + Storage Gateway 
- supports cross-region/account backups
- supports *point-in-time recovery*(PITR) for supported services
- on-demand & scheduled backups
- tag-based backup policies
- create backup policies == **Backup Plans**
  + backup frequency (every 12 hours, daily, weekly, monthly, cron expression)
  + backup window
  + transition to cold storage (Never, days, weeks, months, years)
  + retention period (Always, Days, Weeks, Months, Years)

