# S3 Storage & Data Management for SysOps

## S3 Versioning for SysOps 

- versioning creates new version each time change file, including encrypted files
  + good protection against hackers
- deleting file in bucket only adds delete marker on versioning 
- to delete a bucket, must remove all file versioning within
- encrypting a bucket that has versioning enabled that wasn't encrypted at creation will create new version that is encrypted, original versions will not be encrypted :star:

## MFA-Delete

- forces usr to MFA before doing operations in S3
- to use MFA-Delete, enable Versioning on bucket 
- Need to MFA too:
  + permanently delete object version 
  + suspend versioning on bucket
- don't need MFA too: 
  + enable versioning 
  + list deleted versions 
- **ONLY BUCKET OWNER ROOT ACCOUNT CAN ENABLE/DISABLE MFA-DELETE**
- can only be enabled w/CLI, commands in [mfa-delete.sh](code/s3-advanced/mfa-delete.sh)

### S3 Default Encryption vs Bucket Policies 

- Old way: set bucket policy to refuse any HTTP command without proper headers
- :star: New way: use default encryption option at bucket creation
  + **NOTE**: bucket policies are checked before default encryption 

## S3 Access Logs 

- any request made to S3 from any account, authorized/denied, can be logged to another s3 bucket 
- can be checked w/data analysis tools of Amazon Athena 
- log format found [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/LogFormat.html)
**WARNING** :star:
- do not set logging bucket to be monitored bucket, creates logging loop
  + causes bucket to **grow exponentially**
- enable from bucket settings, and set target bucket
  + bucket ACL auto-configured to include access to S3 log delivery group

## S3 Replication (CRR & SRR)

- versioning must be enabled on source & dest. bucket 
- *Cross Region Replication* (CRR) or *Same Region Replication* (SRR) 
- Buckets can be in different accounts 
- Copying is asynchronous
- S3 must have appropriate IAM permissions
- **CRR Use Cases**: compliance, low latency access, replication across accounts 
- **SRR Use Cases**: log aggregation, live replication betw/prod & test accounts 
- only objects added to bucket **AFTER** activating replication will be replicated
- for DELETE operations: 
  + replicate delete markers is optional setting 
  + Deletes w/a version ID are not replicated to prevent malicious deletes 
- no "chain" replication 
  + Bucket 1 is replicated to bucket 2, bucket 2 replicated to bucket 3
  + Objects created in bucket 1 are not replicated to bucket 3
- when creating the bucket:
  + can apply rules to limit scope of replication 
  + can select your own IAM role of have AWS make it for you
  + can enable delete marker replication 

### Example bucket policicy analysis

[example bucket policies](https://docs.aws.amazon.com/AmazonS3/latest/userguide/example-bucket-policies.html)

**Grant permissions to multiple accounts w/added conditions**

```
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddCannedAcl",
      "Effect":"Allow",
      "Principal": {"AWS": ["arn:aws:iam::111122223333:root","arn:aws:iam::444455556666:root"]},
      "Action":["s3:PutObject","s3:PutObjectAcl"],
      "Resource":"arn:aws:s3:::DOC-EXAMPLE-BUCKET/*",
      "Condition":{"StringEquals":{"s3:x-amz-acl":["public-read"]}}
    }
  ]
}
```
- allow principals from accounts "111122223333" and "444455556666" to do PutObject and PutObjectAcl actions, only if header equals "public-read"

**Limit access to specific IP addresses**

```
{
  "Version": "2012-10-17",
  "Id": "S3PolicyId1",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": [
	       "arn:aws:s3:::DOC-EXAMPLE-BUCKET",
         "arn:aws:s3:::DOC-EXAMPLE-BUCKET/*"
      ],
      "Condition": {
	      "NotIpAddress": {"aws:SourceIp": "54.240.143.0/24"}
      }
    }
  ]
}
```

## S3 Pre-signed URLs

- generate from SDK / CLI
  + downloads == easy, use CLI 
  + uploads == harder, use SDK 
- **DEFAULT VALID TIME**: 3600 seconds :star:
  + can change w/`--expires-in[TIME_IN_SECONDS]` argument
- users w/pre-signed URL inherit GET/PUT permissions of person who generated it
- Use cases:
  + allow only logged-in users to download premium video on S3 bucket 
  + allow an ever changing list of users to download files by generating URLs dynamically
  + allow temporarily a user to upload file to precise location in bucket 
- example commands for [creating pre-signed urls](code/s3-advanced/pre-signed-url.sh)

## AWS CloudFront 

- *Content Delivery Network* (CDN)
- improves read performance, content cached at edge closest to user accessing it
- 216 Point of Presence globally (Edge locations)
- DDoS protection, Shield, AWS Web App firewall integration 
- can expose external HTTPS & talk to internal HTTPS backends
- CloudFront Origins
  + S3 Bucket 
    * distribute files & cache at the edge 
    * enhanced security w/CloudFront *Origin Access Identity* (OAI)
    * CloudFront can be used as an ingress (upload to S3)
  + Custom (HTTP)
    * App Load Balancer
    * EC2 instance 
    * S3 website (must enable bucket as static website)
    * any http backend(possibly on-prem backend)

**AT A HIGH LEVEL**
1. clients make request from an edge location 
2. edge retrieves content from S3 over Private network
3. OAI + S3 bucket policy controls access to S3 
4. Content cached at edge for faster access later

- if using EC2 as an origin:
  + instances **must** be public 
  + must allow edge location IPs in the SG 
- if using ALB as origin:
  + ALB must be public, instances can be private 
  + SG for ALB to EC2 
  + SG for edge public IPs to ALB 
- Geo Restriction
  + Allow/Deny list based on IPs 
  + "country" of origin determined by 3<sup>rd</sup> party Geo-IP database
  + control access to copyright content 
- CloudFront vs S3 CRR 
  + CloudFront ==
    * Global edge 
    * files cached for a TTL 
    * **Great for static content must be available everywhere**
  + S3 CRR 
    * must be setup for each region you want replication to happen
    * files updated near real-time
    * read only 
    * **great for dynamic content that needs low-latency in few regions**

### Building out CloudFront 

1. Create S3 Bucket 
2. Create CloudFront distribution
3. Create OAI 
4. Limit S3 Bucket to be accessed only w/this identity

- Origin domain name == S3 content bucket 
- Restrict bucket access == enables access to bucket only through cloudfront URLs 
  + can get AWS to create OAI & update bucket policy as necessary 
- Cache Behavior settings 
  + set view protocol policy (HTTP & HTTPS / Redirect HTTP > HTTPS / HTTPS only)
  + specify allowed HTTP methods
- creating CloudFront distribution can take some time 
- **NOTE**: before the DNS can propagate, accessing content w/cloudfront URL will redirect directly to S3 URL
  + **BY DESIGN, NOT A BUG**
  + once DNS propagation finished, will be able to access through CloudFront 
  + to see S3 content during this time, need to make S3 content public by disabling Public access settings on the bucket 

## CloudFront Access Logs

- every rqst made to CloudFront can be logged to S3 bucket
- can generate reports on:
  + cache stats 
  + popular objects
  + top referrers
  + usage 
  + viewers
- **TROUBLESHOOTING**
  + CloudFront caches HTTP 4xx & 5xx status codes retured by origin 
    * 4xx error indicates user doesn't have access to bucket(403) OR object usr requesting not found (404)
  + 5xx error: Gateway issues 

### S3 Inventory

- help manage storage 
- audit & report on replication & encryption status of objects 
- **USE CASES**
  + business
  + compliance
  + regulatory needs
- can query all data w/Athena, Redshift, Presto, Hive, Spark...
- supports multiple inventories
- data goes from source bucket => target bucket, requires policy setup 
- creating the inventory will modify the bucket policy for the destination bucket 

## S3 Storage Classes

- S3 Standard - General Purpose
  + 11-9s of durability across multiple AZ 
  + if store 10 million objects on S3, on average can expect lost of single object once every 10K years
  + 4-9s of availability over a year
  + sustain 2 concurrent facility fails 
  + **USE CASES**
    * big data analytics
    * mobile & game applications
    * content distribution 
- S3 Standard - Infrequent Access (IA)
  + data less frequently access, but needs rapid access when needed
  + 11-9s of durability 
  + 3-9s of availability
  + Low cost vs S3 Standard 
  + 2 concurrent facility fails 
  + **USE CASES**
    * data store for disaster recovery, backups
- S3 One Zone - Infrequent Access (IA)
  + same as Standard IA, but stored in single AZ 
  + Same durability in 1 AZ; data lost when AZ destroyed
  + 99.5% availability 
  + Low latency & high througput
  + Supports SSL for data at transit & encryption at rest 
  + low cost vs S3 standard - IA by 20%
  + **USE CASES**
    * secondary backup copies of on-prem data
    * data you can recreate
- S3 Intelligent Tiering
  + low latency & high throughput
  + extra monthly monitoring & auto-tiering fee
  + auto moves objects betw/2 access tiers 
  + same durability as Standard - GP
  + resilient against events that impact entire AZ
  + 3-9s of availability over a given year 
- Glacier
  + low cost for archiving/backup 
  + data retained long term (10s of years)
  + alternative for on-prem mag tape storage 
  + 11-9s of durability
  + cost per storage per month == ($0.004/GB + retrieval cost)
  + item in glacier == *Archive*, up to 40 TB
  + Archives stored in *Vaults*
  + Retrieval options:
    * expedited (1-5 minutes), 0.03$ per GB & 0.01$ per rqst
    * standard (3-5 hours) 0.01$ per GB & 0.05$ per 1000 rqsts
    * bulk (5-12 hours) 0.0025$ per GB & 0.025$ per 1000 rqsts
  + minimum storage duration == 90 days
- Glacier Deep Archive 
  + retrieval options:
    * standard (12 hours)
    * bulk (48 hours)
  + minimum storage duration == 180 days
- table of all storage classes & price comparisons [here](https://aws.amazon.com/s3/storage-classes/)
- can specify storage class from the "Additional upload options" menu at bucket creation 
  + need to specify storage class for each upload 
  + can change the storage class of objects 
- when retrieving from Glacier, can specify how long the data will be available 

### S3 Lifecycle rules 

- can move objects betw/storage classes manually/automated w/*lifecycle configuration*
- **transition actions**: defines when objects transitioned to different storage class 
  + ex.: move objects to Standard IA 60 days after creation, move to Glacier for archiving after 6 months
  + types of actions 
    * move *current* versions 
    * move *previous* versions
- **expiration actions**: objects "expire"(delete) after certain time period 
  + ex.: access log files deleted after 365 days
  + can delete old versions of files in versioned buckets 
  + can delete incomplete multi-part uploads 
  + types
    * expire *current* versions 
    * permenantly delete *previous* versions 
    * delete expired delete markes/incomplete multipart uploads
+ can create rules for certain prefixes(ie: s3://bucket/foobar/\*) & certain object tags

**Scenario 1**

"Your application on EC2 creates images thumbnails after profile photos
are uploaded to Amazon S3. These thumbnails can be easily recreated,
and only need to be kept for 45 days. The source images should be able
to be immediately retrieved for these 45 days, and afterwards, the user
can wait up to 6 hours. How would you design this?"

- Sources images on *STANDARD*, w/lifecycle to transtion to *GLACIER* after 45 days 
- Generated thumbnails on *ONEZONE_IA*, w/lifecycle to expire after 45 days

**Scenario 2**

"A rule in your company states that you should be able to recover your
deleted S3 objects immediately for 15 days, although this may happen rarely.
After this time, and for up to 365 days, deleted objects should be recoverable
within 48 hours."

- enable versioning, "deleted objects" are hidden w/"delete markers" & can be recovered 
- transition "noncurrent" version to *S3_IA* for 15 days
- transition "noncurrent versions" to *DEEP_ARCHIVE* after 15 days and up to 365 days 

### S3 Performance 

- auto scales to high rqst rates, latency 100-200ms
- can achieve at least **3500 PUT/COPY/POST/DELETE & 5500 GET/HEAD rqsts per second per prefix in bucket**
- no limits to number of prefixes in bucket 
- if spread reads across 4 prefixes evenly, can achieve 22,000 rqsts per second for GET and HEAD 

---

**KMS Limitation**

- SSE-KMS can impact performance based on KMS limits
- on upload, calls `GenerateDataKey` KMS API
- on download, call `Decrypt` KMS API\
- count towards KMS quota per second, 5500/10,000/30,000 req/s based on region 
- can request quota increase w/Service Quotas console 

---

**Optimizing S3 Performance**

- Multi-part upload:
  + recommended for files > 100 MB, required for files > 5 GB :Star:
  + parallizes uploads
- S3 Transfer Acceleration (upload only)
  + increases transfer speed by transferring file to AWS edge that will fwd data to bucket in target region 
  + supports multi-part upload 
- Byte-Range Fetches
  + parallelize GETs by rqsting specific byte ranges 
  + better resilience in case of failures
  + Can speed up downloads & retrieve only partial data 

### S3 & Glacier Select 

- retrieve less data w/SQl by doing *server side filtering*
- can filter rows & columns (simple sql statements)
- less network traffic, less client-side SPU 

### S3 Event Notifications 

- can create as many as desired 
- delivered quickly, can take up to a minute
- supports object name filtering 
- **USE CASE**: generate thumbnail of images uploaded to S3
- Notification targets: SNS, SQS, Lambda
- if 2 writes made to single non-versioned object at same time, possible to only sent single notification 
  + to make sure event sent every successful write, enable versioning on bucket 
- creating notification will required modifying access policy to allow S3 bucket to send to notification queue

### S3 Analytics 

- helps determine when to transition objects from *STANDARD* to *STANDARD_IA*
- doesn't work for *ONEZONE_IA* or *GLACIER*
- report updated daily, takes 24-48 hours to generate first 
- helps build Lifecycle rules 

## Glacier 

- Operations 
  + Upload: single or by parts for larger archives(requires using the SDK)
  + Download: start retrival job for archive, glacier preps for download, limited time to download after retrieved
  + delete: use Glacier Rest API / AWS SDK by specifying archive ID
- retrieval options [here](#s3-storage-classes)

### Vault policies & Vault Lock 

- *Vault*: collection of archives :star:
  + every vault has 1 access policy & 1 lock policy 
- Policies written in JSON, similar to bucket policy 
- Lock policy: immutable for regulatory & compliance 
- when doing an expedited retrieval, must purchase capacity units
  + ensures retrieval capacity is available when needed
  + 1 capacity unit == 3 expidited retrievals every 5 minutes, upt o 150 MB/s of throughput 
  + immediately available at purchase for 1 month 
- once the vault lock is initiated, it cannot be changed

## Snowball

- physically transport data (TBs or PBs)
- alternative to moving data over network
- Secure, tamper resistant, KMS-256 bit encryption
- tracking w/SNS, text messages, E-ink shipping label
- payed per data transfer job 
- if network transfer takes > 1 week, use Snowball
- Process
  1. request snowball device from AWS console 
  2. install snowball client on server
  3. connect snowball to servers, copy files w/client 
  4. ship back when done to AWS 
  5. Data loaded into S3 bucket 
  6. Snowball will be completely wiped 
  7. tracking done w/SNS, text, and AWS console 
- *Snowball edge*
  + add computational capability to device 
  + 100 TB capacity w/either:
    * Storage optimized (24 vCPU)
    * Compute optimized (52 vCPU & optional GPU)
  + uses custom EC2 AMI, can perform processing on the go 
  + supports custom Lambda functions 
  + pre-process data while moving 
  + **USE CASES**: data migration, image collation, IoT capture, machine learning
- use snowmobile if need to transfer > 10 PB of data 

**Solution Architecture: Snowball => Glacier**

- Snowball can't dump right to Glacier directly
- have to use S3 first, and an S3 lifecycle policy 

## Hybrid Cloud for Storage 

- "Hybrid Cloud": on-prem + cloud storage 
- Because S3 is proprietary, can't use on-prem
- can use *AWDS Storage Gateway*
  + bridges on-prem & cloud data in s3
  + **USE CASES**: disaster recovery, backup & restore, tiered storage 
- types:
  * File gateway
    + S3 buckets accessible w/NFS or SMB
    + Supports S3 Standard, S3 IA, S3 One zone IA 
    + Bucket access w/IAM roles for each File Gateway 
    + Most recently used data cached in file gateway 
    + can be mounted on many servers
  * Volume Gateway
    + block storage w/iSCSI protocol backed by S3 
    + backed up EBS snapshots, can help restore on-prem volumes 
    + *cached volumes*: low latency access to most recent data 
    + *stored volumes*: entire dataset on-prem, scheduled backups to S3
  * Tape Gateway
    + build Virtual Tape Library (VTL) backed by S3 & Glacier 
    + back up data w/existing tape-based processes
    + works with leading backup software 

**EXAM TIP** 
- On premise data to the cloud => Storage Gateway
- File access / NFS => File Gateway (backed by S3)
- Volumes / Block Storage / iSCSI => Volume gateway (backed by S3 with EBS snapshots)
- VTL Tape solution / Backup with iSCSI = > Tape Gateway (backed by S3 and Glacier)

## AWS Athena 

- **Serverless** solution to do analytics directly on S3 files 
- uses SQL to query files 
- has JDBC/ODBC driver 
- charged **_per query & amount of data scanned_** :star:
- supports: CSV, JSON, ORC, Avro, Parquet(On Presto)
- **USE CASES**: Bus-Int/analytics/reporting, analyze & query VPC flow logs, ELB logs, CloudTrial trails, etc...

**EXAM TIP**
-if analyzing data directly on S3 => use Athena
