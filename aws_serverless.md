# AWS Serverless

## What is Serveless

- just deploy code & dunctions 
- started w/just Lambda, now includes any managed AWS service 
- **SERVERLESS =/= NO SERVERS**: just don't have to manage them 
- Serverless AWS Options
  + Lambda
  + DynamoDB
  + Cognito
  + API Gateway
  + S3
  + SNS & SQS 
  + Kinesis Data Firehose
  + Aurora Serverless 
  + Step Functions
  + Fargate

## AWS Lambda 

| EC2 | Lambda | 
| --- | --- |
| virtual servers in cloud | virtual **functions**, no servers | 
| limited by RAM & CPU | limited by time, **short executions** |
| Continuously running | run **on-demand** |
| scaling means intervention to add/remove servers | **scaling automated** |

- benefits:
  + easy pricing
    * per request & compute time
    * free tier of 1 million lambda requests & 400,000 GBs of compute time
  + integrated w/whole AWS suite
  + supports many programming languages
  + easy monitoring w/CloudWatch
  + can get more resources per functions (<=3 GB of RAM)
  + increase RAM will improve CPU & network 
- Language native support 
  + node.js (JavaScript)
  + Python
  + java
  + C# (.NET core)
  + Golang
  + C#/Powershell
  + Ruby
  + Custom runtime API (community support, Rust)
- **Pricing Example**
  + Pay per **calls**
    * first million free
    * 0.20$ per 1 million requests after (0.0000002 per request)
  + Pay per **duration** (increment of 100ms)
    * 400,000 GB-seconds of computer time per month **FREE**
    * == 400,000 seconds if function uses 1 GB RAM
    * 3,200,000 seconds if function uses 128 MB RAM 
    * after, 1$ for 600,000 GB-seconds
  + usually **very cheap** to run Lambda 
- Limits to know **PER REGION** :star:
  + **Execution**
    * memory allocation: 128 MB - 3008 MB (64 MB increments)
    * max execution time: 900 seconds (15 mins)
    * environment variables (4 KB)
    * Disk capacity in /tmp "function container": 512 MB
    * Concurrency executions: 1000 (can increase)
  + **Deployment**
    * size (compressed .zip): 50 MB
    * uncompressed deployment (code + dependencies): 250 MB
    * use /tmp directory to load other files at startup
    * environment varialbles: 4 KB 

### Lambda@Edge

- CDN w/CloudFront, can deploy Lambda functions on CloudFront CDN w/Lambda@Edge
  + build more responsibe applications
  + no servers, Lambda deployed globally
  + customize CDN content
  + pay for what you use
- Lambda can change CloudFront requests & responses
  + after CloudFront receives rqst from viewer (viewer request)
  + before CloudFront forwards response to viewer (viewer response)
  + before CloudFront forwards request to origin (origin request)
  + after CloudFront receives response from origin (origin response)
  + also generate responses to viewers w/o ever sending rqst to the origin 
- **USE CASES**
  + website security & privacy
  + dynamic web app at edge 
  + search engine optimization (SEO)
  + intelligent routing across origins & data centers 
  + bot mitigation at edge 
  + real-time image transformation
  + A/B testing
  + User authent. & authoriz.
  + User prioritization
  + User Tracking & Analytics 

## DynamoDB 

- fully managed, HA w/replication across 3 AZ
- NoSQL databased (not relational)
- scales very well, distributed database
- millions of rqsts per seconds, trillions of row, 100s of TB of storage
- fast & consistent performance 
- integrated w/IAM for security, authoriz., admin
- enables event driven programming w/DynamoDB Streams
- low cost, auto scales 

### Basics

- made of **tables**
- each table has **primary key** <= assigned at creation
  + table infinite **items (rows)**
- each item has **attributes** (added over time, can be null)
- max item size == **400KB**
- supported data types:
  + Scalar: String,Number,Binary,Boolean,Null
  + Document: List,Map
  + Set: String set,Number set,Binary set

### Provisioned Throughput

- must have provisioned read & write capactiy units
- **Read Capacity Units (RCU)**: throughput for reads ($0.00013 per RCU)
  + 1 RCU = 1 strongly consistent read of 4KB per second
  + 1 RCU = 2 eventually consistent read of 4KB per second
- **Write Capacity Units (WCU)**: throughput for writes ($0.00065 per WCU)
  + 1 WCU = 1 write of 1KB per second
- option to setup auto-scale of throughput to meet demand 
- throughput can exceed temporarily w/"burst credit"
  + if burst credit empty, error "ProvisionedThroughputException"
  + advised to do exponential back-off retry

### DAX

- **DAX = DynamoDB Accelerator**
- seamless cache, no application re-write
- writes go through DAX to DynamoDB
- Micro second latency for cached reads & queries
- solves hot key problem (too many reads)
- 5 minutes TTL for cache by default
- <= 10 nodes in cluster
- Multi AZ (3 node min recommended for production)
- secure (Encryption at rest w/KMS,VPC,IAM,CloudTrail)

### Streams

- changes (Create,Update,Delete) end up in Stream 
- can be read by Lambda and then...
  + react to changes in real time 
  + Analytics
  + Create derivitive tables/views
  + Insert into ElasticSearch
- implement cross region application w/Streams
- 24 hours of Data Retention

### Other Features

- Transactions
  + all or nothing operations
  + coordinate insert, update, delete across multiple tables
  + include upt o 10 unique items/up to 4MB of data
- On Demand
  + no capacity planning needed, auto scale
  + 2.5x more expensive than provisioned
  + helpful when spikes un-predictable/application is low throughput
- **Global Tables**
  + Active-Active replication, many regions
  + requires DynamoDB Streams
  + good for low latency, DR purposes
- **Capacity Planning**
  + planned: provision WCU & RCU, can enable auto scale
  + on-demand: unlimited WCU & RCU, no throttle, more expensive

## Security & Other Features

- **Security**
  + VPC Endpoints available to access DynamoDB without internet
  + full IAM access control
  + encryption at rest w/KMS
  + encryption in transit w/SSL/TLS
- **Backup & Restore**
  + point in time restore 
  + no performance impact
- Global Tables
  + multi region, full replication, high performance 
- AWS DMS(Database migration service) can migrate to DynamoDB (from mongo, oracle, MySQL, S3, etc...)
- launch local DynamoDB on localhost for dev purposes

