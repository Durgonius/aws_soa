# S3 Storage and Data Management

## Buckets 

- store objects(files) in "buckets"(directories)
- must have **globally unique name**
- defined at region 
- Naming convention
  + no uppercase/underscore
  + 3-63 characters long
  + can't be an IP 
  + must start w/lowercase letter/number
- buckets contain **Objects**

### Objects 

- have a key
- key is **FULL** path of file:
- naming convention: prefix + object name 
  + s3://my-bucket/foo/bar/foobar.txt
- no "directory" structure in buckets, just long names that contain slashes
- object values are content of body:
  + :star: Max size: 5 TB
  + uploads > 5 GB must use "multi-part upload"
- objects can have metadata (list of key/value pairs - system/usr metadata)
- tags: unicode key/value pair, up to 10...helpful for security/lifecycle
- Version ID if versioning enabled

### Versioning

- must be enabled at the **bucket level**
- same key overwrite will increment the "version"
- :star: best practice to use versioning 
- any file not versioned prior to enabling will have version "null"
- suspending versioning doesn't delete previous versions
- deleting an object from bucket sets type to `delete marker`
- to permenantly delete an object, you must delete the delete marker + the previous version of the object 

### Encryption for Objects 

- 4 encryption methods:
  + SSE-S3: encrypts w/keys handled & managed by AWS
    * keys handled & managed by S3
    * encrypted server side 
    * AES-256 encryption
    * must set header: `"x-amz-server-side-encryption":"AES256"`
  + SSE-KMS: use AWS Key Management Service to manage encryption keys
    * handled & managed by KMS 
    * KMS advantages: user control + audit trail 
    * server side encryption 
    * header: `"x-amz-server-side-encryption":"aws:kms"`
  + SSE-C: when you want to manage your own keys
    * uses data keys fully managed by customer outside AWS 
    * S3 doesn't store encryption key 
    * **HTTPS REQUIRED**
    * encryption must be provided in HTTP headers for every request made
  + Client Side: 
    * client library needed (Amazon S3 Encryption client)
    * must encrypt before sending & decrypt when retreiving from S3 
    * Customer fully manages keys & encryption cycle
- Encryption in Transit
  + S3 exposes:
    * HTTP endpoint 
    * HTTPS endpoint (encryption in flight)
  + can use endpoint u want, HTTPS recommended
  + most clients use HTTPS endpoint by default 
  + HTTPS mandatory for SSE-C
  + Encryption in flight == SSL / TLS

## S3 Security 

- User Based:
  + IAM Policies: which API calls allowed for certain user from IAM console 
- Resource Based:
  + bucket policies: bucket wide rules from S3 console, allows cross account
  + object ACL
  + Bucket ACL
- **NOTE**: an IAM principal can access S3 object IF:
  + user IAM permissions allow it / the resource policy ALLOWS it 
  + **AND** there's no explicit deny
- networking: supports VPC endpoints(instances in VPC without internet)
- logging & audit: S3 ACL stored in other bucket, API calls loged in AWS CloudTrail 
- User Security: 
  + MFA Delete: MFA needed in versioned bucket to delete objects 
  + pre-signed URLS: valid only for limited time

### S3 Bucket Policies 

- JSON based policies 
  + Resources: buckets & objects 
  + Actions: set of API to allow/deny
  + Effect: allow/deny 
  + principal: account/user to apply policy too 
- Use policy too:
  + grant public access to buck
  + force objects to be encrypted at upload 
  + grant access to another account (Cross account)
- Bucket settings for Block Public Access
  + Block public access to buckets/objects granted through
    * *new* ACL
    * *any* ACL 
    * *new* public bucket/access point policies 
  + block public and cross-account access to buckets & objects through *any* public bucket/access point policies 
  + **created to prevent data leaks**
  + if know bucket should never be public, leave on 
  + can be set at account level 

## S3 Websites

- S3 can host static websites 7 be accessed from Internet
- URl format:
  + <bucket-name>.s3-website-<AWS-region>.amazonaws.com 
- if receiving 403 Forbidden error, make sure bucket policy allows public reads

## CORS

- **origin**: schemed (protocl), host(domain) and port
- CORS == Cross-origin resource sharing 
- Browser based mechanism to allow rqsts to other origins while visiting main origin  
  + EX, same origin: `http://example.com/app1` & `http://example.com/app2`
  + EX, different origin: `http://www.example.com` & `http://other.example.com`
- rqsts not fulfilled unless other origin allows rqsts w/ **CORS Headers (ex: Access-Control-Allow-Origin)**
- if client does cross-origin rqst on S3 bucket, need to enable right CORS headers 
- can allow for specific origin/for *
- things to enable for CORS 
  1. both buckets need to be enabled for static website 
  2. both buckets need to be public 
  3. requests for the resource need to have the full URL for the object in the other bucket 
  4. configure CORS settings on 2nd bucket to allow requests from other origins, example in [cors-config.json](code/s3/CORS_CONFIG.json)

## S3 Consistency Model 

- After a:
  + successful write of a new object (new PUT)
  + or overwrite/delete of existing object (overwrite PUT or DELETE)
- ...any:
  + subsequent read rqst immediately gets latest version of object (**read after write consistency**)
  + subsequent list rqst immediately reflects changes (**list consistency**)

