# EC2 for SysOps 

---

## Cross Account AMI Copy 

- can share AMI w/other AWS account 
- sharing doesn't affect ownership of AMI 
- copying AMI shared w/you, you are owner of target AMI in your account 
- topy an AMI shared w/you, owner of source must grant read permissions to storage that AMI is on (EBS snapshot / S3 Bucket)
- **Limits**
  + cannot copy encrypted AMI shared from another account 
    * if underlying snapshot + encryption key shared, copy snapshot & re-encrypt w/own key (copied snapshot is owned by you, can be registered as new)
  + cannot copy AMI w/`billingProduct` code shared w/you
    * windows AMIs & AMIs from AWS marketplace
    * to copy, launch instance w/shared AMI, create AMI from that instance 

---

## Elastic IPs 

- static public IPv4 
- attached to one instance at a time, can be remapped
- not charged for elastic IP if attached to server 
- delete if not using the IP 
- can hide software/instance failure by remapping to running instance
- try to avoid if possible 

---

## CloudWatch

### Metrics for EC2

- AWS provided metrics (AWS pushed)
  + Basic (Default): collected every 5 mins 
  + Detailed (Paid): collected every 1 min 
  + Included metrics :star: 
    * CPU: usage+credit usage/balance
    * Network: in/out
    * Disk: read/write for ops/bytes (only instance store)
    * Status Check: instance status(EC2 VM), system status(AWS checks underlying hardware)
- Custom metrics (Defined by you) 
  + Basic resolution: every 1 min 
  + High resolution: up to every second 
  + AWS metrics + RAM, application level metrics 
  + :star: make sure IAM permissions on instance role are correct 

### Cloudwatch - Unified Agent

- create new IAM policy for EC2, and assign CloudWatch permissions to it, attach to EC2 instance 
  + important permissions are `cloudwatch:PutMetricData` and `ssm:GetParameter`
** To install Unified CLoudWatch agent **

1. install agent: 
```shell
wget https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm
sudo rpm -U ./amazon-coudwatch-agent.rpm 
```
2. run wizard: 
```shell 
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-config-wizard
```
   - wizard will walk through steps to setup cloudwatch agent and spit out config
   - Assign parameter name for SSM Parameter (DEFAULT: AmazonCloudWatch-linux)
   - May need to attach additional IAM policy that allows `ssm:PutParameter` (CloudWatchAgentAdminPolicy has this)
3. Check Parameters in AWS SSM for your configured parameter 
4. Cloudwatch will not immediately get metrics and parameters 
   - start from config file: `sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:$(config_file_path) -s`
   - start cloudwatch with a fetched config `sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c ssm:$(config-parameter-store-name) -s`
5. May need to create additional files to start the agent (use mkdir & touch)

- systemd service `amazon-cloudwatch-agent.service` will be created, can do usual systemctl stuff

