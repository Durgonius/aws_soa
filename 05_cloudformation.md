# Cloudformation

for managing Ifra as code

- will cut down on manual work required
- declarative way to outline AWS infra for any resource 
- Infra as Code
  + no manual creation
  + version controlled w/git
  + changes are reviewed through code
- Cost
  + resource in stack stagged w/identifier to see how much each stack costs
  + estimate costs of resources w/CloudFormation template 
- Productivity
  + destroy & re-create infra on cloud on the fly
  + auto generate of Diagram of templates 
  + declarative programming
- Separation of concern: create many stacks for many apps & many layers
- Pre-made templates available
- Building blocks :star:
  + Resources: AWS resources declared in template (**MANDATORY**)
  + Parameters: dynamic inputs for template 
  + Mappings: static variables for template 
  + Outputs: references to what has been created 
  + Conditionals: list of conditions to perform resource creation 
  + Metadata

## Creating a Stack

- import from existing templates from AWS
- upload yaml config file 
- specify S3 URL 

## Update & Delete a Stack 

- have to edit the yaml file for the stack and re-upload 
- in CloudFormation console > Update > Replace > Upload new file 
- CloudFormation will show you the differences in the two files

## CloudFormation Parameters 

- provide inputs to AWS CloudFormation template 
- don't have to re-upload template to change its content
- can be controlled by:
  + type (string, num, CommaList, lits, AWS Param)
  + Description
  + Constraints
  + ConstraintDescription
  + Min/MaxLength
  + Min/MaxValue
  + Defaults
  + AllowedValues (array)
  + AllowedPattern (regexp)
  + NoEcho (boolean)
- `!Ref` can be used to reference parameters, can reference other elements/resources in same YAML file 
- Pseudo Parameters: from AWS, available in any CloudFormation template 
  + AWS::AccountID
  + AWS::NotificationARNs
  + AWS::NoValue
  + AWS::Region
  + AWS::StackID
  + AWS::StackName

## CloudFormation Resources

- **MANADATORY** :star:
- represent AWS compoenents to create & configure
- declared & can reference each other 
- Resource identifiers in format: `AWS::aws-product-name::data-type-name`
- 224 resources found [here](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)
- Must be declared, cannot be dynamically created 

## CloudFormation Mappings

- fixed variables in CloudFormation template 
- good to differentiate between envs
- written as:

```yaml
Mappings:
  Mapping01:
    Key01:
      Name: Value01
    Key02:
      Name: Value02
    Key03:
      Name: Value03
```

- Mappings vs. Parameters
  + Mapping when know values that can be taken and greater control needed
  + parameters when values are user specific
- to return named value from specific key: `!FindInMap [ MapName, TopLevelKey, SecondLevelKey ]`

## CloudFormation Outputs 

- *optional* values that can be imported -> other stacks 
- can view outputs in console/AWS CLI
- good way to do cross-stack colab
- can't delete if outputs are being referenced by another stack 
- Example:

```yaml
Outputs:
  StackSSHSecurityGroup:
    Description: The SSH Security Group for our Company
    Value: !Ref MyCompanyWideSSHSecurityGroup
    Export:
      Name: SSHSecurityGroup
```

- to reference an exported value, use `!ImportValue` function

## CloudFormation Conditions 

- control creation of resources/outputs 
- conditions can reference another condition, parameters, mapping
- Example:

```yaml
Conditions:
  CreateProdResources: !Equals [ !Ref EnvType, prod ]
```
- Accepted logical operators for conditions:
  + `!And`
  + `!Equals`
  + `!If`
  + `!Not`
  + `!Or`
- Can be applied to resources/outputs/etc, example:

```yaml
Resources:
  Mountpoint:
    Type: "AWS::EC2::VolumeAttachment"
    Condition: CreateProdResources
```

## CloudFormation Intrinsic Functions

- **MUST KNOW** :star:
  + `!Ref`
  + `!GetAtt`: attributes attached to resources created 
  + `!FindInMap`
  + `!ImportValue`: import values exported in other templates 
  + `!Join`: joins values w/delimeter: `!join [ ":" [a, b, c] ]`
  + `!Sub`: substitue variables from text, can combine with references/AWS Pseudo variables, `!sub string_of_text`
  + Conditionals: logical functions 

## CloudFormation CreationPolicy

- Example:

```yaml
AutoScalingGroup:
  Type: AWS::AutoScaling::AutoScalingGroup
  Properties:
    AvailabilityZones:
      !GetAZs:
        Ref: "AWS::Region"
    LaunchConfigurationName:
        Ref: LaunchConfig
    DesiredCapacity: '3'
    MinSize: '1'
    MaxSize: '4'
  CreationPolicy:
    ResourceSignal:
      Count: '3'
      Timeout: PT15M
```

- `ResourceSignal` tells creation policy to wait for 3 resources to check in within 15 minutes 

## CloudFormation UpdatePolicy 

- example config at [asg-update-policy.yaml](code/cloudformation/from-devops-course/1-asg-update-policy.yml)
- applies to auto scaling group 
- 3 attributes needed:
  + [Rolling Update](code/cloudformation/from-devops-course/2-asg-update-policy-rolling.yml) or 
  + [Replace update](code/cloudformation/from-devops-course/3-asg-update-policy-replace.yml)
  + `AutoScalingScheduledAction`
- without specifying, only the launch config will be changed 

---

# SysOps Specific

### CloudFormation User Data 

- pass entire user data script through function `!Base64`
  + output of script goes too `/var/log/cloud-init-output.log`
  + syntax is `!Base64: | `, past script after pipe, allows for new-line

### CloudFormation cfn-init 

- `AWS::CloudFormation::Init` in metadata of resource 
- makes EC2 config readable 
- EC2 instance queries CloudFormation service to get init data 
- logged too `/var/log/cfn-init.log` 
- example: 

```yaml
UserData:
  !Base64:
    !Sub |
      #!/bin/bash -xe
      # get latest cloudformation package 
      yum update -y aws-cfn-bootstrap
      # Start cfn-init 
      /opt/aws/bin/cfn-init -s ${AWS::StackID} -r MyInstance --region ${AWS::Region} ||
      error_exit 'Failed to run cfn-init`
```

1. update cloudformation package
2. start cfn-init with...
   + Stack ID from AWS Pseudo variable 
   + retrive cfn-init metadata from MyInstance resource 
   + apply to region from AWS Pseudo variable 
3. in metadata, can define packages, files & content, commands, etc.. (refer to [cfn-init.yml](code/cloudformation/4-cfn-init.yaml) for more info)

### cfn-signal & wait conditions 

- run `cfn-signal` after `cfn-init`, tell CloudFormation service to keep going or fail 
- define WaitCondition, blocks template until receives from `cfn-signal`
  + attach CreationPolicy
- check [cfn-signal.yaml](code/cloudformation/5-cfn-signal.yaml) for syntax that is added

Error message `Wait condition didn't receive required number of signals from Amazon EC2 Instance`
- make sure AMi has AWS CloudFormation helper script installed, can be downloaded if missing
- verify `cfn-init` & `cfn-signal` successfully run on instances, view logs at `/var/log/cloud-init.log` or `/var/log/cfn-init.log`
- can retrieve logs by logging into instance, but must disable rollback on fail or else AWS cloudformation deletes instance after stack fails to create
- verify instance has internet connection, otherwise can't talk to cloudformation service 
  + failed deployment script in [cfn-signal-failure.yaml](code/cloudformation/6-cfn-signal-failure.yaml)

### CloudFormation rollbacks 

- stack creation fails:
  + default: everything rolls back (gets deleted) 
  + can disable & troubleshoot
- Stack update fails:
  + auto rolls back to previously known working state 
  + can check log 

### Nested Stacks 

- allow to isolate repeated patterns / common resources in seperate stacks, call from other stacks 
- considered best practice :star: 
- to update a nested stack, need to update the parent stack 
- configration file: [nestedstacks.yaml](code/cloudformation/7-nestedstacks.yaml)
  + needs `TemplateUrl` 
  + template URL may need parameters passed too it 
- nested stack will finish first, then parent 
- deleting a parent stack will delete the nested stack 

### ChangeSets 

- either re-use current, replace current, edit template in designer
- gives small degree of version control between template changes 
- preview changes before commiting them to the stack 

### CloudFormation Deletion Policy

- control what happens when template is deleted 
- `DeletionPolicy=Retain`: 
  + specify resources to preserve/backup 
- `DeletionPolicy=Snapsho`:
  + EBS Volume, ElastiCache cluster, ElastiCache ReplicationGroup
  + RDS DBInstance, RDS DBCluster, Redshift Cluster
- `DeletionPolicy=Delete`(Default Behavio):
  + *NOTE*: AWS::RDS::DBCluster resources default is Snapshot
  + *NOTE*: to delete S3 bucket, need to empty the bucket first 
- example in [deletionpolicy.yaml](code/cloudformation/9-deletionpolicy.yaml)
- termination protection does similar, just need to enable/disable on the stack 
