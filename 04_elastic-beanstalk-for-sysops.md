# Elastic Beanstalk for SysOps 

## Overview

- developer centric view of deploying application on AWS 
- free, pay for underlying instances 
- managed service
  + instance config...OS handled by EBS 
  + Deployment strategy configurable, done by EBS 
- only application code is handled by dev 
- 3 Architecture models:
  + single instances deployment: good for dev 
  + LB + ASG: great for prod/pre-prod web apps
  + ASG only: great for non-web apps in prod (workers, etc...)
- 3 Components 
  + application 
  + application version 
  + environment name 
- app versions deployed to environments, can promote app versions to next environment 
- rollback feature to previous application version 
- full control over lifecycle of environment
- supported platforms: 
  + Go 
  + Java SE & Java w/Tomcat
  + .NET on win server w/IIS 
  + Node.js
  + PHP 
  + Python
  + Ruby 
  + Packer Builder
  + Single container docker 
  + multicontainer docker
  + preconfigured docker
  + custom platform, written by you 
- when creating an environment, can specify platform branch & version 

## Deployment Modes 

- **All at once**: fastest, downtime 
- **Rolling**: updates a few at a time(bucket), moves onto next bucket after is healthy
  + runs at below capacity
  + can set bucket size
  + if small bucket, with lots of instances, deployment can take a while
- **Rolling w/additional batches**: like rolling, creates new instances to move the batch
  + runs at capactiy w/set bucket size 
  + deploys new instances w/new app ver 
  + extra instances terminated to return to capacity 
  + extra cost, good for prod 
- **Immutable**: makes new instance in new ASG, deploys version, swaps all instances when all healthy 
  + zero downtime 
  + deployed to new instances on temp. ASG 
  + high cost, double capacity, longest time, quick rollback 
  + great for prod 
- blue/green 
  + create new "stage" env, deploy v2 in stage 
  + stage is validated independently
  + Route 53 setup w/weighted policies to redirect low traffic to stage 
  + w/Beanstalk, "swap URLs" when testing done, stage becomes prod 
- bucket size can be set as percentage or fixed size 

## Beanstalk for SysOps 

- can put app logs --> CloudWatch Logs 
- you manage app, AWS handles infra 
- know different deployment modes for app 
- custom domain: Route 53 ALIAS/CNAME on top of Beanstalk URL
- AWS handles patching runtimes 
- :star: How Beanstalk deploys apps 
  1. EC2 has base AMI
  2. EC2 gets new code of app 
  3. EC2 resolves app dependencies (can take time)
  4. apps get swapped on EC2 instances 
- how do you deploy without waiting for dependencies?
  * **THE GOLDEN AMI**: you create a standardized company-specific AMI w/:
    + Package OS dependencies
    + Package App dependencies
    + Package company-wide software 
  * w/Golden AMI to deploy EBS, app doesn't need to resolve dependencies

**Troubleshooting**

- if health of env changes to red:
  + review env events
  + pull logs to view recent log file entries 
  + rol back to previous, working ver of app 
- make sure security groups configured to allow access to external resources 
- increase deployment time to allow for command timeouts

