# Managing EC2 at Scale - SSM & Opswork 

## AWS Systems Manager 

- manage EC2 & on-prem at scale 
- operational insights about state of infra 
- find issues 
- auto-patching for enhanced compliance 
- Windows & Linux 
- cloudwatch & AWS config integration
- FREE $$$ 

- Features
  + Resource Groups 
  + Insights
    * insight dashboard
    * inventory: find & audit software installed
    * compliance 
  + Parameter Store
  + Action  
    * automation 
    * **run command**
    * session manager 
    * **patch manager** 
    * maintenance windows 
    * state manager 

---

### Start EC2 instance w/SSM Agent

- Amazon Linux instance has SSM agent installed by default 
- Requires IAM role that can communicate w/SSM (`AmazonEC2RoleforSSM` is default role)
- SSM Managed instances don't need SSH in their security groups 
- instances must have right IAM **&** SSM agent installed to appear under SSM Managed Instances tab 

---

## AWS Tags & SSM Resources 

- Text-Key Value Pairs
- more tags is better 
- Resource Groups 
  + create/view/manage groups of resources by tag 
  + regional service
  + works w/EC2, S3, Dynamo, Lambda

---

### SSM Documents & Run Command 

- Document:
  + json or yaml
  + defined parameters 
  + defined actions 
  + AWS has pre-written documents
  + can be used w/ state manager/patch manager/automation/run command & can reference the parameter store

- Run command 
  + run documents/commands across multiple instances(resource groups)
  + rate control/error control
  + IAM & CloudTrail integrated 
  + SSH not needed
  + outputs to web console/write S3 bucket/CloudWatch

### SSM Inventory & Patches

- inventory > list software on instance 
- inventory + run > patch software 
- patch manager + maintenance window > patch OS 
- Patch Manager > gives you compliance 
- State manager > ensures instances in constant state of compliance

### Session Manager 

- can create secure shell for VM, **doesn't need SSH access/bastion** 
- only EC2 for now 
- log actions done w/secure shells to S3 & CloudWatch logs 
- IAM Permissions needed: access SSM + write to S3 + write to CloudWatch :star:
- CloudTrail can intercept StartSession events 

### Lost EC2 SSH Key 

- (traditional) if isntance EBS backed 
  + stop instance, detach root volume 
  + attach root volume to another instances as data valume 
  + modify the `.ssh/authorized_keys` file w/new key 
  + move volume back to stoppped 
  + start instance & SSH
- (New Method) if instance is EBS: 
  + run `AWSSupport-ResetAccess` automation document in SSM 
- Instance Store backed EC2 
  + can't stop instance (data lost) ... AWS says terminate 
  + w/Session Manager, can access & edit `~/.ssh/authorized_keys` file directly 

---

## SSM Parameter store 

- securely store configuration & secrets 
- Seamless Encryption w/KMS 
- Serverless, scalable, durable, easy SDK, free
- Version tracking of config/secrets 
- Config management using path & IAM 
- CloudWatch Events notifications
- CloudFormation integration 
- hierarchical


**Using SSM Parameter store from CLI**

- to get parameters from SSM store
```shell
aws ssm get-parameters --names path/to/parameter /path/to/another/parameter
```
  + secure strings will not be displayed decrypted, must specify `--with-decryption` to show
  + requires KMS access to see SecureStrings 
- to get all parameters from SSM store in a path 
```shell
aws ssm get-parameters-by-path --path /path/to/directory/
```
  + returns all items in directory 
  + `--recursive` does what you think it does 
  + `--with-decryption` accepted

### AWS Opsworks 

- Chef & Puppet works great to manage server config 
- AWS Opsworks = Managed Chef & Puppet 
- **any reference to chef & puppet ==> AWS Opsworks** 

