# AWS Monitoring, Audit, and Performance 

---

## Monitoring in AWS

- CloudWatch
  + Metrics: Collect & track key metrics
  + Logs: Collect, monittor, analyze & store log files
  + events: send notifications when certain events happen in AWS
  + Alarms: React in real-time to metrics 
- CloudTrail 
  + internal monitoring of API calls being made 
  + audit changes to AWS resources
- Config
  + Track configuration of resources over time
  + evaluate config compliance based on rules

## CloudWatch

### CloudWatch Metrics 

- Metrics for *every* service in AWS 
- **Metric** == variable to monitor
- metrics part of **namespace**
- **dimension** == attribute of metric (instance ID, environment, etc...)
  + up to 10 dimensions per metric 
- metrics have **timestamps**
- can create CloudWatch dashboards of metrics

### CloudWatch EC2 Detailed monitoring 

- metrics **_EVERY 5 MINUTES_** :star:
- w/detailed monitoring (for cost), **_EVERY 1 MINUTE_** :star:
- use detailed monitoring to better scale w/ASG
- AWS Free allows <= 10 detailed monitoring metrics 
  + **NOTE**: EC2 memory usage by default not pushed, must be pushed from instance as custom metric :star:

### CloudWatch Custom Metrics 

- can define & send custom metrics 
- use dimensions to segment metrics 
- Metric resolution (**StorageResolution** API parameter, 2 values):
  + Standard: 1 minute 
  + High Res. : 1 second (more expensive)
- requires permission to do **PutMetricData**
- use exponential back off in case of throttle errors

### CloudWatch Dashboards

- **DASHBOARDS GLOBAL**
  + can include graphs from different regions 
  + can change time zone & time range of dashboards 
  + setup auto refresh (10s, 1m, 2m, 5m, 15m)
- Pricing:
  + 3 Dashboards(Max 50 Metrics) free
  + 3$/Dashboard/month after

### CloudWatch Logs 

- logs to CloudWatch w/SDK from Application
- Can collect from:
  + Beanstalk
  + ECS
  + Lambda
  + VPC Flow Logs
  + API Gateway
  + Cloudtrail based on filter 
  + CloudWatch log agents
  + Route53
- CloudWatch logs can go to 
  + Batch exporter to S3 for archival
  + Stream to ElasticSearch cluster for analysis 
- logs storage architecture 
  + Log Groups: arbitrary name, usually == application
  + log stream: instance within application / log files / container
- can defined log expiration policies 
- tail CloudWatch logs w/AWS CLI 
- if sending logs to CloudWatch, make sure IAM permissions are right 
- KMS log encryption at group level 
- Support filter expressions
  + metric filters can be used to trigger alarms 
- CloudWatch logs insight, used to query logs & add queries to CloudWatch Dashboards

### CloudWatch Alarms 

- trigger notification for any metric
- can send to Auto Scaling, EC2 Actions, SNS Notifications
- Options (Sampling, %, max, min, etc...)
- States:
  + OK
  + INSUFFICIENT_DATA
  + ALARM
- Period
  + Length of time in seconds to evaluate metric
  + high rest custom metrcis: can only choose 10 / 30 seconds 
- Alarm targets:
  + stop,terminate,reboot,recover an EC2 instance 
  + trigger auto scaling action 
  + send notification to SNS 
- Things to know :star:
  + alarms can be created based on CloudWatch logs metric filters 
  + CloudWatch doesn't test/valide actions assigned
  + test alarms/notifications, to set alarm state to Alarm w/CLI: `aws cloudwatch set-alarm-state --alarm-name "myalarm" --state-value ALARM --state-reason "testing alarm"`

### CloudWatch Events 

- source + rule => target
- schedule: cron jobs
- Event pattern: event rules to react to service doing something
- triggers lambda FN, SQS/SNS/Kinesis Messages
- CloudWatch Event creates small JSON document for info about change 

---

## AWS CloudTrail 

- **governance, compliance, audit for AWS account**
- enabled by default 
- get history of vents from AWS account by:
  + Console
  + SDK 
  + CLI
  + AWS Service
- Put logs from CloudTrail => CloudWatch logs/S3
- **Supports Global(default) & single region**
- if resource deleted in AWS, check CloudTrail first 

### CloudTrail  Events

- events stored for 90 days :star:
  + to keep events > 90 days, log to S3 & use Athena
- **Management Events**:
  + operations done on resource in AWS account
  + Examples:
    * configurating security (IAM `AttachRolePolicy`)
    * Configuring rules for routing data (Amazon EC2 `CreateSubnet`)
    * Setting up logging (AWS CloudTrail `CreateTail`)
  + **BY DEFAULT**: trails configured to log management events :star:
  + can sperate **read events**(don't modify resources) from **write events**(modify resources)
- **Data Events**: 
  + **BY DEFAULT**: data events not logged 
  + S3 object-level activity (`GetObject`, `DeleteObject`, `PutObject`) seperate Read & Write events 
  + AWS Lambda function execution activity (`Invoke` API)
- CloudTrail Insights

### CloudTrail Insights 

- not enabled by default :star:
- enable **to detect unusual activity**
  + inaccurate resource privisioning
  + reaching service limit
  + bursts in IAM actions
  + gaps in periodic maintenance activity
- Insights analyzes normal management events to make baseline
- **continuously analyzes _write_ events to detect unusual patterns**
  + anomalies appear in console
  + event sent to S3
  + EventBridge event generated 

---

## AWS Config 

- auditing & compliance for AWS resources 
- record configs & changes over time
- record compliance over time
- can store AWS config -> S3 (Queried w/Athena)
- AWS Config solves:
  + Is there unrestricted SSH access to my SGs?
  + Do my buckets have any public access?
  + How has my ALB config changed over time?
- can receive SNS alerts for changes
- per-region service, can be aggregated across regions & accounts 

### Config Rules

- AWS managed config rules
- supports custom config rules(defined w/Lambda), example:
  + evaluate each EBS disk of type gp2
  + evaluate each EC2 instance of type t2.micro
- rules evaluated w/triggers:
  + for each config change 
  + and/or at regular time intervals
- Pricing:
  + no free tier
  + ~$2 USB per active rule per region per month (less after >10 rules)
- Config resource:
  + view compliance of resource over time
  + view configuration of resource over time 
  + View CloudTrail API calls if enabled 

---

## CloudWatch vs CloudTrail vs Config 

- CloudWatch 
  + Performance monitoring & dashboard 
  + Events & Alerting
  + Log aggregation & analysis 
- CloudTrail 
  + Record API calls made in Account by everyone 
  + define trails for specific resources
  + Global Service 
- Config
  + Record configuration changes 
  + Evaluate reosurces against compliance rules
  + Get timeline of changes & compliance
- **EXAMPLE**: For an ELB... :star:
  + CloudWatch:
    * monitoring incoming connections metric 
    * visualize error codes as a % over time
    * make dashboard to view load balancer performance
  + Config: 
    * track SG rules for LB
    * track config changes for LB 
    * ensure SSL cert always assigned to LB(compliance)
  + CloudTrail
    * Track who made changes to LB w/API calls 
