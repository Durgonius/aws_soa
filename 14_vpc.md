# VPC 

--- 

# Summary
- CIDR: IP range
- VPC: Virtual Private Cloud => defined list of IPv4 & IPv6 CIDR
- Subnets: tied to AZ, we define CIDR
- Internet Gateway: at VPC level, provide IPv4 & IPv6 Internet access
- Route tables: add routes from subnets to IGW, VPC Peering connections, VPC endpoints, etc...
- NAT Instances: provide internet access to instances in private subnets, old, must be setup in public subnet, disable Source/Destination check flag
- NAT Gateway: AWS managed, scalable internet access to private instances, IPv4 only
- Private DNS + Route 53: DNS resolution + DNS hostnames (VPC)
- NACL: stateless, subnet rules for in/outbound traffic, don't forget ephemeral ports
- Security Groups: stateful, operate at EC2 instance level
- VPC peering: connect 2 VPC w/non-overlapping CIDR, non-transitive
- VPC Endpoints: Provide private access to AWS services within VPC without traversing public internet
- VPC flow logs: setup at VPC/Suibnet/ENI level, identify problem traffic
- Bastion host: public instance to SSH then access internal instances
- Site to site VPN: setup customer gateway on DC, Virtual Private Gateway on VPC, site-to-site VPN over public internet
- Direct connect: setup Virtual Private Gateway on, create direct private connection to AWS Directo Connect Location
- Direct Connect Gateway: setup Direct Connect to many VPC in different regions
- Internet Gateway Egress: NAT Gateway for IPv6

---

## Default VPC

- all new accounts have default VPC
- new instance launched into default if no subnet specified
- has internet connectivity, all instances has public IP
- get public & private DNS name

### VPC in AWS 

- VPC == *Virtual Private Cloud*
- max 5 VPCs (Soft limit) per region 
- Max 5 CIDR per VPC, each CIDR:
  + min size == /28
  + max size == /16
- Only private IP ranges are allowed (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16)
- **VPC CIDR should not overlap w/other networks IE. corporate**

### IPv4 Subnets

- AWS reserves 5 IPs (first 4, last 1 in each subnet)
  + X.X.X.0: Network address
  + X.X.X.1: AWS reserved, VPC router
  + X.X.X.2: AWS reserved, Amazon-provided DNS mapping
  + X.X.X.3: AWS reserved, future use
  + X.X.X.255: Broadcast address, not supported in VPC by AWS 
- **EXAM TIP**: if need 29 addrs for EC2 instances, can't choose subnet size /28, only gives 16 IPs, need /26

--- 

## Internet Gateways

- help VPC instances connect w/internet
- scales horizontally, HA & redundant
- must be created seperately from VPC 
- 1 VPC per IGW 
- Internet gateway does NAT for instances
- require Route tables to access internet

### NAT Instances (outdated, still on exam)

- allows instances in private subnets to connect to internet 
- launched in public subnet 
- must disable EC2 flag: Source/Destination check
- must have elastic IP attached
- route table configured to route traffic from private subnets to NAT instance 
- Some things about NAT instances:
  + Amazon Linux AMI pre-configured available
  + not HA/Resilient setup ootb
  + needs ASG in multi AZ + good user-data script 
  + internet traffic bandwidth depends on EC2 instance performance
  + inbound SG requirements:
    * Allow HTTP/HTTPS from Private Subnet
    * allow SSH from home network 
  + Outboung SG requirements
    * allow HTTP/HTTPS to internet 

### NAT Gateway 

- AWS managed, higher bandwidth, better availability, no admin
- pay by hour for usage & bandwidth
- NAT created in specific AZ w/EIP
- cannot be used by instance in same subnet 
- needs IGW (private subnet => NAT => IGW)
- 5 Gbps b/w w/auto scaling to 45 Gbps 
- no SG needed to managed 
- NAT gateway is resilient within **singl-az**
  + requires multiple NAT gateway in muli-az for fault-tolerance
  + no cross AZ failover needed (if az goes down, doesn't need NAT)

---

## DNS Resolution in VPC

- `enableDnsSupport`: (resolution setting)
  + default true
  + decide if DNS resolution supported for VPC
  + ifTrue, queries AWS DNS at 169.254.169.253
- `enableDnsHostname`: (hostname setting)
  + default false for new VPC, default true for default VPC
  + nothing happens unless `enableDnsSupport=true`
  + ifTrue, assign public hostname to EC2 instance if has public IP 
- **if using custom DNS domain names in private zone w/Route53, must set both attributes to true**

---

## Network ACLs & SG

- incoming request: NACL inbound rules = pass => SG inbound rules = pass => EC2 :star:
  + outbound return request is auto-allowed at SG (**STATEFULL**), NACL outbound rules process (**STATELESS**)
- outgoing request: SG outbound rules = pass => NACL outbound rules = pass => out 
  + inbound return request stateless at NACL inbound, statefull at SG inbound 
- default NACL allows everything outbound & everything inbound 
- **ONE NACL PER SUBNET**, new subnets assigned DEFAULT NACL 
- Standard NACL list rules (processed top down, 1-32766)
- Newly created NACL deny everything 
- greate way to block specific IP at subnet level

### NACLS vs. Security Groups

| Security Group | Network ACL |
| --- | --- |
| operates at instance level | operates at subnet level |
| supports allow rules only | supports allow & deny rules |
| statefull: return traffic auto allowed, regardless of rules | stateless: return traffic explicitly allowed by rules |
| evaluate all rules | process rules in number order |
| Applies to instance only if specified when launching instance / associates SG w/instance | auto applies to all instances in subnets associated with |

- example for working with ephemeral ports [here](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports)

--- 

## VPC Peering

- connect two VPC privately w/AWS' network 
- requires non-overlapping VPC 
- peering is **not transitive** (full mesh basically)
  + Example: VPC has A has peering to VPC B. VPC B has peering to VPC A & C. For VPC A to connect to VPC C, must create a new peering 
- inter-region, cross account 
- can reference SG of peered VPC 
- both route tables must be updated 

---

## VPC Endpoints

- allow AWS services to connect w/private network 
- scales horizontally & is redundant
- remove need for IGW, NAT, to access AWS services 
- **interface**: provisions ENI(w/private IP) as entry point (requires SG), supports most AWS services 
- **Gateway**: provisions target, must be used in route table, for S3 & DynamoDB
- in case of issues:
  + check DNS setting resolution in VPC
  + Check route tables 

### PrivateLink 

- Interface Endpoints powered by PrivateLink 
  + privaly copnnect VPC to other AWS services
  + services hosted by other AWS accounts
  + supported marketplace partner services
- doesn't need IGW, NAT device, public IP, AWS Direct Connect, AWS Site-to-Site VPN to communicate w/service
- if you are "service provider", can create VPC Endpoitn Service powered by PrivateLink to have other accounts connect to your application

---

## Flow Logs

- capture info about Ip traffic going into interfaces
  + VPC Flow 
  + Subnet FLow
  + Elastic Network Interface flow
- monitor & troubleshoot connectivity issues
- flow logs data can go to S3 / CloudWatch logs 
- captures network info from AWS managed interfaces too: ELB, RDS, ElastiCache, Redshift, WorkSpaces 
- Syntax: `<version><account-id><interface-id><srcaddr><dstaddr><srcport><dstport><protocol><packets><bytes><start><end><action><log-status>`
  + Srcaddr,dstaddr help identify problem IP
  + srcport,dstport identify problem port
  + Action: success/fail of request due to SG/NACL
- Query VPC flow logs w/Athena on S3/CloudWatch Logs insight :star:
- Troubleshoot SG vs NACL issue?
  + incoming requests
    * inbound REJECT: NACL or SG
    * Inbounc ACCEPT, outbound REJECT: NACL
  + Outgoing request 
    * outbound REJECT: NACL or SG
    * Outbound ACCEPT, inbound REJECT: NACL 

--- 

### Bastion Hosts

- bastion is in public subnet, then connects to all other private subnets 
- **Bastion SG must be tightened**
- **EXAM TIP**: make sure bastion host only has port 22 traffic from IP you need, not from SGs of other instances 

---

## Site to Site VPN

- Virtual private Gateway
  + VPN Concentrator on AWS side 
  + VGW created & attached to VPC to be the source 
  + Can customize ASN
- Customer gateway
  + software/physical device on customer side of VPN 
  + IP: needs static public IP for customer gateway, if CGW behind NAT needs public IP of NAT 

### Direct Connect

- dedicated *private* connection from remote network to VPC
- must be setup betw/DC & AWS direct connect locations
- need to setup Virtual Private Gateway on VPC
- access public resource (S3) & private (EC2) on same connection 
- **USE CASES**
  + increase bandwidth throughput, work w/large data sets at low cost 
  + consistent network experience, applications w/real-time data feeds
  + hybrid environments
- IPv4 & IPv6
- **Direct Connect Gateway**: to Direct Connect to one or more VPC in different regions (Same Account), must use direct connect gateway
  + does not peer VPCs together
- **Connection Types**
  + dedicated: 1 Gbps, 10 Gbps capacity
    * physical ethernet port dedicated to customer
    * request made to AWS first, completed by AWS Direct connect partner
  + Hosted: 50 Mbps, 500Mbps, to 10 Gbps
    * connection requests made through AWS Direct connect partners
    * capacity can be **added/removed on demand**
    * 1,2,5,10 Gbps available from select partners
- lead times longer than 1 month to make new connection
- **Encryption**
  + data in transit **NOT ENCRYPTED**, but is private
  + direct connect + vpn == IPsec tunnel
  + good for extra security, more complex to build

### Egress only Internet Gateway

- egress only gateway for IPv6 only
- gives IPv6 instances access to internet, but won't be directly reachable by internet 
- edit route tables after creating 
