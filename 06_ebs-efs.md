# EC2 Storage & Data management

## EBS Volumes 
 
- network drive, can be attached/detached to different instances
- locked to an AZ, can't be moved without snapshotting
- capacity (GBs & IOPS) provisioned at creation
  + billed for all provisioned capacity 
  + can increase over time 
- 4 types:
  + GP2(SSD): general purpose, balanced price & performance
  + IO1(SSD): high perf SSD, low-latency/high throughput workload
  + STI(HDD): low cost HDD, frequently accessed, high throughput workloads 
  + SCI(HDD): lowest cost, less frequently accessed, cold storage
- **ONLY GP2 & IO1 CAN BE USED AS BOOT VOLUME**

### Setup EBS volume inside EC2 Linux

1. `lsblk`
2. `sudo file -s /dev/xvdb`: identify existing contents of disk 
3. `sudo mkfs -t ext4 /dev/xvdb`: create ext4 filesystem on the device
4. `sudo mkdir /data`: create mountpoint named `data` for the disk
5. `sudo mount /dev/xvdb /data`: attach disk to mount point

- do usual `/etc/fstab` junk to make sure it mounts at boot 

## EBS Volume Types

- GP2
  + recommended for most workloads
  + 1GiB - 16 TiB
  + Small volumes can burst to 3000 IOPS
  + max IOPS is 16000
  + 3 IOPs per GB, max IOPS at 5334 GiB 
- IO1
  + critical business apps, need sustrained IOPS performance 
  + 4 GiB - 16 TiB 
  + IOPS is provisioned: MIN 100, MAX 64,000(Nitro instances only), others MAX 32,000
  + max ratio of provisioned IOPS to requested volume size in GiB is 50:1
- ST1
  + streaming workloads, need consistent, fast throughput at low price 
  + can't be boot volume 
  + 500GiB-16TiB
  + Max IOPS is 500
  + Max through of 500 MiB/s, burstable
- SCI 
  + throughput-oriented storage for large volumes of data, infrequently accessed 
  + cannot be boot volume 
  + 500 GiB-16TiB 
  + max IOPS is 250 
  + max throughput of 250 MiB/s, burstable 

### EBS Volume Burst

- if GP2 volume less than 1000 GiB, can burst up to 3000 IOPS 
- accumulate burst credit over time
- larger the volume, faster burst credit gain
- if burst credit balance empty:
  + max I/O becomes baseline paid for on storage
  + if balance constantly 0, increase GP2 volume/switch to IO1
  + use CloudWatch to monitore I/O credit balance 
+ ST1 & SC1 can also burst for increased throughput 

### Computing EBS Throughput

- GP2: throughput in MiB/s = (Volume size in GiB) x (IOPS per GiB) x (I/O size in KiB)
  + limit to max of 250MiB/s (ie: volume >= 334 GiB won't increase throughput)
- io1: througput in MiB/s = (Provisioned IOPS) x (I/O size in KiB)
  + throughput limit of io1 volumes is 256 KiB/s for each IOPS provisioned
  + limit to max of 500 MiB/s (at 32,000 IOPS) and 1000 MiB/s (at 64,000 IOPS)

### EBS Volume Resizing

- only increase volume size 
- must repartition after resizing 
- volume may be in "optimization" phase, still usable at lower performance

### EBS Snapshots

- incremental: only backup changed blocks
- backups use IO, run during low traffic time 
- snapshots stored in S3
- don't have to detach volume to snapshot, recommended
- max 100,000 snapshots per account 
- can copy across AZ/Region 
- can create AMI from snapshot
- **EBS volume restored by snapshot need to be pre-warmed w/fio or DD to read entire volume**
- **Automate w/Amazon Data Lifecycle Manager**
  + use Lifecycle policies to schedule snapshots & retention policies 
  + supports IAM, tags

### EBS Migration

- volumes locked to specific AZ
- to migrate to different AZ/Region
  + Snapshot volume 
  + copy volume to different region 
  + create volume from snapshot in AZ

### EBS Encryption

- when creating encrypted EBS volume: 
  + Data at rest encrypted inside volume
  + all data in flight between instance & volume is encrypted 
  + All snapshots encrypted 
  + all volumes created from snapshot encrypted
- encryption & decryption are transparent, no action needed
- minimal performance impact
- EBS encryption keys use KMS (AES-256)
- Copying unencrypted snapshot allows encryption 
- shapshots of encrypted volumes are encrypted 
- to encrypt EBS volume:
  1. Create EBS snapshot of volume
  2. encrypt EBS snapshot w/copy 
  3. Create new ebs volume from snapshot 
  4. Attach encrypted volume to the original instance 

## EBS vs Instance Store 

- instance store is physical attached volume, is ephemeral storage 
- Pros:
  + better I/O performance 
  + good for buffer/cache/scratch data/temp content 
  + data survives reboots
- Cons
  + on stop/terminate, instance store is lost 
  + can't resize
  + backups must be done by user
- disks up to 7.5 TiB, can be stripped to reach 30 TiB 
- block storage 
- data can be lost due to hardware failure

## EBS for SysOps 

- to use root volume of instance after terminated, set delete on termination flag to no
- if using EBS for high performance, use EBS-optimized instance types :star:
- don't over-provision EBS volume, need to pay for unused storage 
- **Troubleshooting**
  + High wait time/slow response for SSD == increase IOPS 
  + EC2 won't start w/EBS volume as root, make sure volume names mapped properly
  + after increasing volume size, must repartition to use the newly expanded storage 

### EBS RAID

- EBS already redundant, can use RAID to increase IOPS or mirror EBS volumes 
- RAID 0 Example: two 500 GiB EBS io 1 volumes w/4000 provisioned IOPs creates -> 1000 GiB RAID 0 array w/8000 IOPS total & 1000 MB/s througput 
- RAID 1 Example: two 500 GiB EBS io 1 volumes w/4000 provisioned IOPS creates -> 500 GiB RAID 1 array w/4000 IOPS and 500 MB/s of throughput 
- done through console of instance, can't be done in AWS console 

### CloudWatch & EBS 

- Important Metrics:
  + VolumeIdleTime: # of seconds when no read/write sent
  + VolumeQueueLength: # of ops waiting to be executed, high number means an IOPS or application issue
  + BurstBalance: if 0, need volume w/more IOPS
- Metric intervals:
  + GP2 types, 5 minutes 
  + io 1, 1 minute 
- status check:
  + OK
  + Warning 
  + impaired
  + Insufficient-data

## EFS - Elastic File System 

- managed NFS that can be mounted on many EC2 
- works w/instances in multi-AZ
- very expensive, pay per use, scales automatically 
- uses NFSv4.1
- security group for access control 
- **LINUX AMI ONLY**
- encryption at rest w/KMS 
- POSIX file system, has standard file API
- Performance and classes
  + Scale
    * 1000s of concurrent NFS clients, 10 GB+/s throughput 
    * can scale to petabtye sized network file system
  + Performance mode(set at creation time)
    * General purpose (*default*):star:: latency-sensitive use cases 
    * Max I/O: higher latency, throughput, highly parallel
  + Tiers (lifecycle management feature)
    * Standard: frequently access files 
    * EFA-IA (Infrequent access): cost to retrieve files, lower price to store 

