# Practice Exam Mistakes

- automate process of creating & managing IAM roles for multiple AWS accounts, w/AWS Organizations already in use
  + CloudFormation StackSets w/AWS Organizations to deploy & managed IAM roles to multiple AWS accounts simultaneously
- root user of AWS account doesn't have access to S3 bucket in their company's AWS account 
  + if IAM user w/full access to IAM & S3 assigns bucket policy to S3 bucket & doesn't specify AWS account root user as principal, root user denied access 
- application w/complex runtime & OS dependencies taking long time in Beanstalk. Cannot sacrifice application availability
  + new beanstalk environment for each applicaation, apply blue/green deployment patterns
  + create Golden AMI for application 
- secure CloudTrail logs
  + use CloudTrail log file integrity to keep logs tamper-proof
    * hashed & signed w/SHA256 & RSA 
- gp2 drive of 8TB reached peak performance of 10,000 IOPS & fully utilized, how to increase performance & keep same cost
  + 2 4TB gp2 drives, mount in RAID 0 
- replace on-prem NFS v3 drive w/solution to use S3 capacity, how to ensure files commonly used cached locally?
  + File Gateway: SMB/NFS access w/local caching
  + Volume gateway :x: : represents cloud-backed iSCSI block storage, doesn't replace on-prem NFS v3 drive 
- website hosted in S3 & exposed w/CloudFront distribution, users reporting lots of 501 errors, how to analyze errors?
  + CloudFront access logs w/Athena: CloudFront access logs already in an S3 bucket, use athena for athena things 
- large company w/several AWS accounts, problem w/Sharing CMKs across accounts for accessing resources, solution?
  + Key policy for CMK must give external acocunt(/users & roles in external account) permissions to use CMK :white_check_mark:
  + IAM policies in external account must delegate key policy permissions to users & roles :white_check_mark:
- is customer data secure while in-transit & at rest in cloud while using Storage gateway?
  + :white_check_mark: Storage Gateway uses SSL/TLS in transit, uses S3 managed encryption keys for server-side encryption of all data stored in S3 
- company wants to archive 5 PB of data from aon-prem to durable long-term cloud storage, solution?
  + on-prem data => snow edge storage optimized devices, copy into S3, create lifecycle policy to move data to Glacier :white_check_mark:
  + :x: can't copy snoball data directly into glacier :star:
- have tape backup process, would like to migrate to cloud for S3 storage capacity w/same process & iSCSI compatible backup software, solution?
  + :white_check_mark: tape gateway: supports backup applications, cached virtual tapes on-prem, data encrypted in-transit
- what is important to remember when configuring retention periods for objects in S3 buckets (id [object lock](https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-lock-overview.html#object-lock-retention-periods))
  + different version of single object can have different retention modes & periods 
  + when apply retention period to object verion explicitly, you specify `retain until date` for object version
- What does `DisableApiTermination`(termination protection) attribute control?
  + :white_check_mark: prevents whether instance can be terminated w/console, CLI, or API
  + :x: : does not prevent from terminating by shutting down from within the instance
  + :x: : does not prevent ASG from terminating instances
- if EBS volume reports `error` status, what does this mean & what can be done?
  + :white_check_mark: : underlying hardware fialure w/EBS volume. can't be recovered, but can be restored from backup 
  + :x: : restarting instance won't help here, neither will lifecycle manager 
- AWS Systems Manager feature to help w/config management, software audit & integration w/AWS Config?
  + :white_check_mark: AWS Systems Manager Inventory
  + :x: : patch manager only manages patching instances w/security-related updates for OS
- what is true about deleted items in a versioning-enabled bucket?
  + :white_check_mark: delete marker set on deleted object, actual object not deleted 
  + :white_check_mark: `GET` requests do not retrieve delete marker objects 
- CloudWatch Metric to monitor when either an Instance check or System Status check fails?
  + :white_check_mark: `StatusCheckFailed`
- How do you ensure only CloudFront can access files in an S3 bucket?
  + :white_check_mark: use Origin Access Identity & bucket policy 
  + :x: cannot associate IAM role to CloudFront, anything that says you can is wrong
  + :x: cannot attach security group to S3/CloudFront
- attempting to recover an AMI that was deleted, what are some ways to do this?
  + :white_check_mark: Create new AMi from EBS snapshot created as backups 
  + :white_check_mark: Create new AMI from EC2 instances launched before deletion of AMI
  + :x: can't recover/restore deleted/deregistered AMI, **YOU MUST MAKE A NEW ONE**
- a note about ELBs and AWS X-Ray 
  + :white_check_mark: ALBs do not send data to X-Ray, will not appear as node for X-Ray
- create an AMI of existing EC2 instances that is application-consistent?
  + :white_check_mark: create by disabling `No reboot` option, default action during AMI creation is to shutdown (Bad), OS buffers not flushed to disk before snapshots created 
- what is a good reason to use EC2 enhanced networking?
  + :white_check_mark: support throughput near/exceeding 20K packets per second (PPS) on VIF driver
- you have two config files for  CloudWatch agent with different configurations, they have the same file name, but are stored in different paths. What will happen if the CloudWatch agent is started w/the first config file & the second is appended to it?
  + :white_check_mark: append overwrites information from first config file instead of appending
    * make sure file names are different if they are on the same server 
  + :x: CloudWatch only supports one config file <= wrong, can have multiple
- attaching EBS volume to EC2 instance, but in "attaching" state for long time, what is cause?
  + :white_check_mark: ensure device not already in use, attempt to attach in volume to instance again but w/different name 
- You have configured CloudWatch billing alarms for instances running in eu-west-2, however no information is visible when accessing billing information & alarms. Why is this?
  + :white_check_mark: billing & alarm data can be accessed only from us-east1 region
- by default, what status message will you see if your CloudFormation stack encounters error during creation?
  + :white_check_mark: `ROLLBACK_IN_PROGRESS`, CloudFormation tried to create resources, failed, rolls back resources, exits with fail `ROLLBACK_COMPLETE`
- an application is deployed in several AWS regions worldwide, with microservices in EC2 instances in each region & Amazon Aurora in a single region. What architecture will provide highest performance for end users?
  + :white_check_mark: Amazon Route 53 record set for applications w/latencty routing policy, deploy ALB in front of EC2 instances in each region
    * ALB better for microservice arch, targets can be registered as specific port on EC2 instance
  + :x: geolocation routing probably won't provide better performance
- if you require a storage solution that supports: scalability, a hierarchical directory structure, POSIX permissions control, what is the best choice?
  + :white_check_mark: EFS 
  + :x: not EBS, provides block storage volumes
- you have an IAM role attached to an EC2 instance, and it needs to access objects in an S3 bucket. It is able list objects, but is denied when trying to get them. What can be done to resolve this?
  + :white_check_mark: verify bucket policy allows IAM role to get objects for resource `arn:aws:s3:::BucketName/*` and check if IAM role allows `s3:GetObject` action for S3 bucket resource
- what services does CloudWatch use to send email following an alarm event?
  + :white_check_mark: SNS, can be used to send email 
- if your application stores logs to a folder inside ephemeral storage of an EC2 instance, but it is experiencing issues and you are unable to access the logs due to instances being deleted by ASG downscaling, how can you collect the logs better in the future?
  + :white_check_mark: install & set up CloudWatch logs agent in EC2 instances, the logs will auto sync to CloudWatch so they can be analyzed after instances are terminated 
  + :x: S3 bucket options are wrong, cannot be attached to EC2 instances as storage source
- if you need to monitor a monolithic application in EC2 and serverless application in Lambda, which AWS services can you use to do this?
  + :white_check_mark: AWS X-Ray supports monitoring for both of these architectures
  + :x: CloudTrail if for auditing API call logs, doesn't analyze code itself
- what type of policies can grant permissions to IAM users?
  + :white_check_mark: IAM policy & S3 bucket policy
  + :x: SCPs can limit permissions w/allow & deny rules, but don't grant permissions 
- how would you limit access to only two users for an S3 bucket?
  + :white_check_mark: create a bucket policy explicitly granting access to the two principals
  + :x: explicite deny for all IAM users would work, but is not most efficient 
  + :x: Bucket ACL only apply to bucket objects, not the bucket itself 
- you are configuring an IAM role for an RDS database to read & write bucket objects. You have an IAM role for EC2 that can perform this, but when you add the role to the RDS db, the role cannot be found. Why is this and how can you fix it?
  + :white_check_mark: trust entity of role is service of EC2 instead of RDS, create new role for RDS service to assume
    * trust identity of IAM role must include `rds.amazonaws.com` for DB to assume it. 
  + :x: policies of IAM role do not have "principal" part 
- a server running in an ASG is terminating & relaunching every few minutes, what is the most likely cause?
  + :white_check_mark: the autoscaling health check is marking the instance as unhealthy before it has time to initialize fully ]
    * caused if Load balancer health check not configured properly 
    * if instance missing SG rules, ALB cannot do health checks
    * health checks too aggressively mark instances as unhealthy 
  + :x: launch config using unsupported AMI for your AZ, the ASG would not even be able to create the instances
- you are trying to reduce your AWS bill by cleaning resources no longer in use, which resources can contribute to monthly AWS bill?
  + :white_check_mark: EBS volume not attached to instance, public IP not allocated to instance, NAT gateway not processing traffic
  + :x: SQS queue not receiving messages doesn't incur cost, VPCs do not have a cost (only the resources inside)
- one of your EC2 instances has a public IP, and you want to make sure that only SSH traffic from a single IP address is allowed to access it, and deny all other public traffic. How would you accomplish this?
  + :white_check_mark: use an NACL on the subnet for the EC2 instance 
  + :x: an SG does not support explicit DENY rules
- can AWS trusted advisor identify security issues within your EC2 instances?
  + :white_check_mark: no, it can only identify security issues with AWS accounts 
- you would like to monitor your EC2 instances' memory usage using the CloudWatch agent. Do you need to configure a custom script for this?
  + :white_check_mark: no, memory usage is one of the [supported metrics](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/metrics-collected-by-CloudWatch-agent.html) collected by the CloudWatch agent 
- A user is using AWS SQS, what operations are **NOT** supported by SQS?
  + :white_check_mark: `ReadMessage`, not a valid operation, closest is `receive-message`

