# AWS Account Management

---

## AWS Status - Service Health Dashboard

- show all regions, all services health
- historical info for each day 
- supports RSS feed to subscribe to
- [AWS Status](https://status.aws.amazon.com)

### Personal Health Dashboard

- Global service 
- shows how AWS outages directly impact you
- shows impact on resources
- lists issues & actions to remediate
- [AWS Personal Health Dashboard](https://phd.aws.amazon.com)

---

## AWS Organizations

- global service
- allows management of multiple AWS accounts
- main account == master account, can't change 
- other accounts == member accounts 
- member accounts can only be part of 1 organization
- consolidated billing, single payment method
- pricing benefits from aggregated usage
- API available to automate AWS account creation 

### Multi Account Strategies

- create accounts per *department*, per *cost center*, per *dev/test/prod* environment, based on *regulatory restrictions*, for *better resource isolation*, to have *seperate per-account service limits*, isolated account for *logging*
- Multi account vs. One account, Multi VPC
- use tagging standards for billing purposes
- enable CloudTrail on all accounts, log to central S3 account 
- send CloudWatch logs to central logging account
- Cross Account Roles for admin purposes
- Organized w/**OUs**
  + OUs inherit security rules

### Service Control Policies (SCP)

- allow/denylist IAM actions
- applied at **OU**/**Account** level 
- doesn't apply to master account
- SCP applied to all **Users & Roles** of account, including root user
- SCP does not affect service-linked roles
  + service-linked roles allow other services to integrate w/AWS Org, can't be restricted by SCPs
- SCP must have explicit allow (Deny by default)
- **USE CASES**
  + restrict access to certain services
  + Enforce PCI compliance by explicitly disabling services 
- OU SCPs will override Account SCPs 
- deny takes priority

### Moving Accounts 

- to migrate accounts from one org to another...
1. remove member account from old org
2. send invite to new org 
3. accept invite to new org from member account

- if want to migrate mast of old org to new org...
1. remove all member accounts from org
2. delete old org
3. repeat process to invite old master account to new org

---

## AWS Service Catalog

- users new to AWS have too many options, may creates stacks not compliant/in line w/rest of org
- usrs just want **self-service portal** to launch set of **authorized products** pre-defined **by admins**
- includes: VM, DB, storage, etc...
  + users pick from product list, choices authorized by IAM, creates ready to use provisioned products 
  + Admin builds the product CloudFormation templates, combine products into portfolio, define IAM access to portfolio 
- create and manage catalogs for IT services approved on AWS
- "products" == CloudFormation templates
- CloudFormation ensures consistency, standardization by Admins
- assigned to Portfolios (teams)
- teams present self-service portal where can launch products 
- all deployed products centrally managed deployed services
- helps governance, compliance, consistency 
- give user access to launch products without needing deep AWS knowledge
- integrates w/"self-service portals"(ServiceNow)

---

### AWS Billing Alarms

- **Billing data metric store in CloudWatch us-east-1**
- billing data for overall **worldwide** costs
- actual cost, not projected 

### AWS Cost Explorer

- graphical tool, view & analyze costs & usage
- review charges & cost associated w/AWS account/org
- forecast spending for next 3 months
- recommendations for which EC2 Reserved Instances to purchase
- access default reports
- API to build custom cost management applications

### AWS Budgets

- create budget, **send alarms when costs exceeds budget**
- 3 types: Usage, Cost, Reservation
- For Reserved Instances (RI):
  + track usage 
  + supports EC2, ElastiCache, RDS, Redshift
- up to 5 SNS notifications per budget
- filter by: Service,  Linked Account, Tag, Purchase Option, Instance Type, Region, Availability Zone, API Operations, etc...
- same options as Cost Explorer
- 2 budgets free, 0.02$/day/budget 

### AWS Cost Allocation Tags

- tags can track resources related to each other
- *cost allocation tags* allow detailed costing reports
- **Like tags, show as columns in reports**
- AWS Generated Cost Allocation Tags
  + Auto applied to resource created
  + Stars w/ **aws:** prefix
  + No applied to resources created before activation
- User Tags:
  + user defined
  + prefix **user:**
- **Cost allocation tags appear in billing console**
- up to 24 hours for tags to show on report 
