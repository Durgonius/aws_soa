# EC2 High Availability and Scalability 

## Elastic Load Balancing
- ELB = server that forward traffic to instances downstream 
  + gives even load 
  + single point of DNS to application 
  + automatically handles failures of downstreams instances 
  + health checks 
  + HTTPS for websites 
  + Stickiness w/cookies 
  + HA across zones 
  + seperate public traffic from private traffic 
- health checks: let ELB know if instances are only & can reply to requests 
  + response 200 = OK 
  + if doesn't receive, instance marked unhealthy 
- Types: 
  + Classic (v1-old-2009) HTTP, HTTPS, TCP
    * TCP/HTTP based health checks 
    * fixed hostname (xxx.region.elb.amazonaws.com)
  + Application (v2-new-2016): HTTP, HTTPS, WebSocket
    * layer 7 only 
    * load balance to multiple HTTP applications across machines (target groups)
    * load balance to multiple applications on same machine (containers)
    * http/2 & WebSocket support 
    * http redirects 
    * routing tables to different target groups (path in URL, hostname in URL, query string/headers in URL)
    * great for micro services & container-based applications 
    * fixed hostname 
    * prevents application servers seeing IP/Port/protocol directly (store in X-Forwarded-For, X-Forwarded-Port, X-Forwarded-Proto)
  + Network load balancer (v2-new-2017): TCP, TLS, UDP
    * forward tcp/udp traffic to instances 
    * lowest latancy
    * **1 static IP per AZ**, supports elastic IP 
    * high performance TCP/UDP traffic
  + can be either private / public 
- Can configure security groups that allow traffic from any to load balancer and only allow load balancer traffic to instances 
- requires "warm up" period before scaling
- Troubleshooting codes:
  + 4xx errors: client induced
  + 5xx errors: application induced 
  + 503 errors ELB at capacity/no registered target 
  + If can't connect to application, check security groups 
- Monitoring:
  + ELB access logs show all requests 
  + CloudWatch metrics give aggregate stats 

---

### Stickiness 

- same client gets same instance from load balancer
- CLB & ALB 
- uses cookie w/expiration date 
- helps users maintain their session data
- may cause load imbalance in backend instances 

---

### SSL/TLS 

- allows traffic betw/clients & oad balancer to be encrypted in transit 
- SSL(Secure Sockets Layer)
- TLS(Transport Layer Security): newer SSL 
- TLS prefered, often just called SSL 
- Public SSL certs issues by CAs, have expiry date & be renewed 
- CLIENT -- HTTPS --> Load blancer -- HTTP --> EC2 instance 
  + ELB uses X.509 cert 
  + manage certs w/ACM(AWS Cert Manager)
  + Can upload own certificates 
  + HTTPS Listener: 
    * **MUST** specify default cert
    * can add option list of certs for multiple domains 
    * **CLIENTS MUST USE SNI(SERVER NAME IDENTIFICATION) TO SPECIFY HOSTNAME TO REACH**
    * can specify security policy to support older versions of SSL/TLS 
- SNI (Server Name Identification): solves problem of loading multiple SSL certs onto one web server to serve multiple websites 
  + requires client indicate hostname of target in initial SSL handshake 
  + ALB, NLB, CloudFront only 

---

### Connection Draining

- CLB = Connection Draining 
- Target Group == Deregistration delay (ALB & NLB) 
- time to complete "in-flight requests" while instance is de-registering/unhealthy
- stops sending new requests to instance de-registering 
- between 1-3600 seconds, 0 == disabled

---

### ELB for SysOps

- ELB scale to traffic, but won't scale fast for a spike 
- must open ticket w/AWS to pre-warm ELB
  + duration of traffic 
  + expected requests per second 
  + size of request (in KB)
- Error Codes 
  + Successful: Code 200 
  + Client error: 4XX Code 
    * 400: bad request 
    * 401: Unauthorized
    * 403: forbidden 
    * 460: Client closed connection
    * 453: X-Forwarded for header w/ >30 IP (Malformed request)
  + Server errors: 5xx code 
    * 500: internal server error (ELB error)
    * 502: Bad Gateway
    * 503: Service Unavailable
    * 504: Gateway timeout (server issue)
    * 561: Unauthorized
- Supporting SSL for OLD browsers 
  + Change policy to allow for weaker cipher (DES-CBC3-SHA)
  + ELBSecurityPolicy-TLS-1-0-2015-04 <- policy that allows weaker cipher 
- Troubleshooting
  + Check security groups 
  + health checks
  + Sticky sessions can bring "imbalance"
  + for Multi-AZ, make sure cross zone balancing enabled 
  + use **Internal** ELB for private apps that don't need public access 
  + Enable deletion protection to stop accidental deletes on critical infra

---

## Metrics, Logging, Tracing for ELBs 

- all ELB Metrics pushed to CloudWatch Metrics 
  + BackendConnectionErrors
  + Healthy/UnHealthyHostCount
  + HTTPCode_Backend_2xx: successful request
  + HTTPCode_Backend_3xx: Redirected request
  + HTTPCode_ELB_4xx: Client error codes
  + HTTPCode_ELB_5xx: server error codes from load balancer 
  + Latency
  + RequestCount
  + **SurgeQueueLength**: total # of rqsts/connections pending routing to healthy instance, helps to scale out ASG, max value == 1024 
  + **SpilloverCount**: total # of rqsts rejected because surge queue is full 
- Access Logs: stored in S3 
  + Time 
  + Client IP 
  + Latencies 
  + request paths
  + Server response 
  + trace ID
- only have to pay for S3 storage of Access logs (must specify location when enabling access logging)
- good for compliance 
- can keep access data after ELB/EC2 instances terminated 
- Access logs already encrypted 

---

## Auto Scaling Groups 

- scale out to match increased load
- scale in to match decreased load 
- mantain min/max number of instances running 
- auto-register new instances to load balancer 
- ASG attributes:
  + launch configuration
    * AMI + instance type 
    * EC2 User data 
    * EBS Volumes 
    * Security Groups 
    * SSH Key pair 
  + Min Size / Max Size / Initial Capacity 
  + Network + Subnets information 
  + load balancer information 
  + Scaling Policies
- can scale ASG based on CloudWatch alarms
- Auto Scaling New Rules
  + Target Average CPU Usage 
  + # of rqsts on ELB per instance 
  + average network in 
  + average network out 
  + Custom metric from CloudWatch 
  + based on schedule 
- IAM roles attached to ASG assigned to EC2 instances 
- ASG restarts/re-creates deleted instances if necessary
- can terminate instances marked unhealthy by an LB 
- :star: If ASG is healthy, but ELB is not healthy, change health check type to ELB 
- **Scaling Processes in ASG**: can be suspended if necessary 
  + launch: add new EC2 to group
  + terminate: removes EC2 instance from group 
  + healthcheck: checks health of instances 
  + ReplaceUnhealthy: terminate unhealthy & re-create
  + AZRebalance: balance EC2 instances across AZ
    * if suspend launch process, AZRebalance won't launch/terminate instances 
    * if suspend terminate process, ASG can grow up to 10% of size, could remain at increased capacity because can't terminate instances 
  + AlarmNotification: Accept notification from CloudWatch
  + ScheduledActions: Performs scheduled actions you create 
  + AddToLoadBalancer: Adds instances to ELB/Target Group

### Notes for SysOps 

- to ensure HA, have at least 2 instances across 2 AZ in ASG 
- health checks: EC2 Status & ELB Health
- ASG will launch new instnace after terminating unhealthy one 
- ASG will not reboot unhealthy hosts 
- CLI to know: `set-instance-health` & `terminate-instance-in-auto-scaling-group`

**Troubleshooting tips**
- \<number of instances\> already running, launching EC2 instance failed 
  + ASG reached limit set by DesiredCapacity parameter, update ASG w/new value for desire capacity 
- Launching EC2 instance failing 
  + security group doesn't exist, SG may be deleted
  + key pair doesn't exist, may be deleted 
- ASG fails to launch instance for 24 hours, will auto suspend process (admin suspension)

---

### CloudWatch for ASG 

- Metrics Available: 
  + GroupMinSize
  + GroupMaxSize
  + GroupDesiredCapacity
  + GroupInServiceInstances
  + GroupPendingInstances
  + GroupStandbyInstances
  + GroupTerminatingInstances
  + GroupTotalInstances
- enable metric collection, collected every minute 

