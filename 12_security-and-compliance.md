# Security & Compliance

---

# SUMMARY

- Shield: Auto DDoS Protection + 24/7 support
- WAF: Web App. Fireall, filter incoming rqsts based on rules
- Inspector: EC2 only, install agent, find vulnerabilities
- GuardDuty: Find malicious behavior w/VPC,DNS, CloudTrail logs
- Trusted Advisor: Analyze AWS account, get security recommendations
- KMS: Encrypted keys managed by AWS
- CloudHSM: hardware encryption, user managed keys, supports asymmetric encryption
- STS: generate security token
- Identity Federation: SAML 2.0/Custom for enterprise, cognito for apps
- Artifact: Compliance reports for AWS 
- Config: track config changes & compliance against rules
- CloudTrail: track API calls made by users in account

---

## AWS Shared Responsibility Model

- AWS responsibility: Security **of** cloud :star:
  + protect infra that runs AWS services
  + managed services 
- Customer responsibility: security **in** the cloud :star:
  + For EC2, customer responsible for guest OS management, firewall, network, IAM, etc...
- **EXAMPLE**: for RDS...
  + AWS handles...
    * manage underlying EC2, disable ssh
    * auto DB/OS patching
    * audit instance & disks to guarantee it functions
  + User responsibility:
    * check ports/IP/SG inbound rules 
    * in-database user creation & permissions
    * Create DB with/without public access
    * ensure parameter groups/DB configured to only allow SSL
    * DB encryption
- **EXAMPLE**: for S3:
  + AWS handles:
    * guarantee unlimited storage
    * guarantee encryption
    * ensure seperation of data betw/customers
    * ensure AWS employees can't access your data
  + User handles:
    * bucket config
    * bucket policy/public setting
    * IAM user & roles
    * enable encryption

---

## What is a DDOS attack?

- distributed denial-of-service 
- DDOS protection on AWS
  + AWS Shield Standard: protects from DDOS on your website/applications, for all customers at no extra cost
  + AWS Shield Advanced == 24/7 premium DDoS Protection
  + AWS WAF: filter specific requests based on rules
  + CloudFront & Route 53:
    * Availability proteciton w/global edge network
    * combined w/AWS Shield, gives attack mitigation at the edge 
  + prepare to scale w/ASG
  + seperate static resources from dynamic ones
  + a handy [whitepaper](https://dl.awsstatic.com/whitepapers/Security/DDos_White_Paper.pdf)

### AWS Shield

- **Shield Standard**
  + Free, active for every AWS customer
  + protects from attacks (SYN/UPD floods, reflection attacks, other layer 3/layer 4)
- **Shield Advanced**
  + Optional DDoS mitigation service (3000$ per month, per organization)
  + Protect against more sophisticated attack on CloudFront, Route 53, classic/app/network load balancer, elastic IP/EC2
  + **24/7 access to DDoS Response team**
  + protect against higher fees during usage spikes due to attacks

### AWS WAF - Web Application Firewall

- protects web applications from common web exploits
- customizable web security rules
  + control which traffic to allow/block to applications 
  + rules can include: IP addr, HTTP headers, HTTP body, URI strings
  + common attack protection (SQL injection, XSS)
  + protect from bots, bad user agents, etc...
  + size constraints
  + geo match
- deploy on CloudFront, ALB, API Gateway
- use existing marketplace of rules

### Penetration Testing

- no permission needed for:
  + EC2 Instances, NAT Gateways, ELB
  + RDS
  + CloudFront
  + Aurora
  + API Gateways 
  + Lambda & Lambda edge functions
  + Lightsail resources
  + Elastic Beanstalk environments
- prohibited activities
  + DNS zone walking w/Route 53 hosted zones
  + DoS, DDoS, simulated DoS/DDoS 
  + Port flooding
  + protocol flooding
  + request flooding
- should not test on t3.nano, t2.nano, t1.micro, m1.small
- for other simulated events, contact amazon

---

## Encryption

- Encryption in flight (ssl)
  + data encrypted before sending & decrypted after receiving
  + SSL certs help w/encryption (HTTPS)
  + erncryption in flight ensures no MITM attack can happen
- Server side encryption at rest 
  + data encrypted after being received by server 
  + data decrypted before being sent 
  + store in encrypted state w/key
  + encryption/decryption keys must be managed somewhere & server must have access 
- client side encryption 
  + data encrypted by client & never decrypted by server
  + data decrypted by receiving client 
  + server should not be able to decrypt data
  + leverage envelope encryption

## AWS KMS (Key Management Service)

- KMS fully integrated into AWS 
- easy to control access to data, AWS manages keys 
- fully integrated w/IAM for auth
- seamless integration w/
  + EBS encrypted volumes
  + S3 Server side object encryption 
  + redshift data encryption 
  + RDS data encryption 
  + SSM parameter store 
  + etc...
- can also use CLI/SDK
- use KMS to share sensitive info 
- value in KMS is that CMK used to encrypt data never retrieved by user, CMK can be rotated for extra security 
- **NEVER STORE SECRETS IN PLAINTEXT, ESPECIALLY IN CODE**
- encrypted secrest stored in code / environment variables 
- **KMS can only hlep in encrypting <= 4KB of data per call**
  + use envelope for data > 4 KB
- to give KMS access to someone:
  + make sure Key Policy allows user
  + Make sure IAM policy allows API calls 
- KMS does...
  + fully manage ekys & policies (create, rotation policies, disable, enable)
  + audit key usage (CloudTrail)
  + 3 types of *Customer Master Keys* (CMK)
    * AWS Managed Server default CMK (Free)
    * User keys created in KMS ($1/month)
    * User keys imported (must be 256-bit symmetric key): 1$/Month
  + pay for API call to KMS (0.03$/10000 calls)
- some services require migration through snapshot/backup
  + EBS Volumes
  + RDS databases
  + ElastiCache
  + EFS network file system
- S3 has in-place encryption, no migration necessary

---

## CloudHSM

- KMS == AWS manages software for encryption
- CloudHSM == AWS provisions encryption **hardware**
- *hardware security module* (HSM)
- can manage own encryption keys entirely 
- CloudHSM device is tamper resistant
- FIPS 140-2 Level 3 compliance :star:
- CloudHSM clusters spread across multi AZ 
- supports symmetric & asymmetric encryption (SSL/TLS keys)
- not free, must use CloudHSM Client software 
- deploy/manage from VPC, shared across VPCs w/VPC peering 

---

## IAM + MFA 

- MFA adds extra security level while accessing AWS account
- accepts both virtual & hardware MFA 
  + virtual: google authenticator, authy
  + hardware: ubikey, trezor, etc..
- MFA for root configured from IAM dashboard
- can be configured from CLI
- MFA for Inidividual users
- Credentials report
  + CSV report on all IAM users & credentials
  + shows all who have MFA

### IAM PassRole :star:

- to assign a role to an EC2, your account requires `IAM:PassRole`
- Can be used for any service where we assign roles 
- PassRole defines the roles your account can assign to an EC2

---

### AWS Inspector

- only for EC2
- analyze against known vulnerabilities, unintended network accessibility
- **requires agent install on guest OS**
- define template (rules package, duragion, attributes, SNS topics), no custom rules 
- after assessment, get report
- What does it evaluate?
  - network reachability
  - common vulnerabilities & exposures
  - Center for internet Security (CIS) benchmarks
  - security best practices
  - runtime behavior analysis
- can install w/ssm run command or regular linux commands 

### Logging in AWS for Security & Compliance 

- service logs include:
  + CloudTrail trails (all API calls)
  + config rules (config & compliance over time)
  + CloudWatch logs (for full data retention)
  + VPC Flow logs (IP traffic in VPC)
  + ELB Access logs (metadata of rqts made to LB)
  + CloudFront logs (web distribution access logs)
  + WAF Logs (full logging of all rqsts analyzed)
- **logs can be checked w/Athena if stored in S3**
- **encrypt in S3, control access w/IAM & Bucket Policies, MFA**
- **Move logs to Glacier to save $$$**

### GuardDuty

- intelligent threat discovery to protect AWS account
- machine learning, anomaly detection, 3rd party data
- 1 click enable for 30 day trial, no installation
- input data includes:
  + CloudTrail logs (Unusual API calls, unauthorized deployments)
  + VPC Flow Logs (Unusual internal traffic, unused IP addrs)
  + DNS logs (compromised EC2 sending encoded datat in DNS queries)
- notifies in case of findings
- integration w/Lambda 

### Trusted Advisor

- high level AWS account assessment
- analyze AWS account & provide recommendation
  + cost optimization
  + performance
  + security
  + fault tolerance
  + **service limits**
- core checks & recommendations (all customers)
- **enable weekly email notification from console**
- full trusted advisor == business & enterprise support plans
  + can set CloudWatch alarms when reaching limits 

## STS & Cross Account

- Security Token Service 
- **Grant limited & temporary access to AWS resources**
- valid <= 1 hour
- **CrossAccountAccess**
  + Allow users one AWS account access resource in another 
- **Federation (AD)**
  + allow non-AWS user w/temp AWS access by linking users AD credentials
  + uses SAML (Security Assertion Markup Language)
  + Allows SSO, enables users to log into AWS console without assigning IAM creds
- **Federeation w/3rd Party providers / Cognito**
  + mainly web/mobile applications
  + use Facebook/Google/Amazon etc to federate 

### Identity Federation 

- allows users outside AWS assume temporary role for accessing AWS resources 
- users assume identity provided access role :star:
- **Federation assumes form of 3rd party auth**
  + LDAP
  + MS AD/SAML 
  + SSO
  + Open ID
  + Cognito
- w/Federation, don't need to create IAM users...usr management outside AWS 
- SAML Federation (Enterprises)
  + integrate AD / ADFS w/AWS (or any SAML 2.0)
  + provdes access to AWS Console/CLI 
  + Don't need to make IAM user for each employee
- Custom Identity Broker Application (Enterprises)
  + use only if identity provider not SAML 2.0 compatible
  + **ID broker must determine right IAM policy**
- AWS Cognito (Public Applications)
  + Goal: provide direct access to AWS resources from client
  + How:
    * login to federated ID provider / remain anon
    * get temp AWS creds back from Federated ID Pool
    * Creds come from pre-defined IAM policy defining permissions
  + **EXAMPLE**: provide temp access to write to S3 bucket w/Facebook login
  + Note: **Web ID Federation is alternative to Cognito, but AWS advises against it** ... just use cognito :star:

## Compliance Frameworks

- Global
  + CSA (Cloud Security Alliance Controls)
  + ISO 9001 (Global Quality Standard)
  + ISO 27001 (Security management Controls)
  + ISO 27017 (Cloud Specific Controls)
  + ISO 27019 (Personal Data protections)
  + PCI DSS Level 1 (Payment Card Standards)
  + SOC 1 (Audit Controls Report)
  + SOC 2 (Security, availability, confidentiality report)
  + SOC 3 (General Controls report)
- USA
  + CJIS (Criminal Justice Information Services)
  + DoD SRG (DoD data processing)
  + FedRAMP ( Gov. Data standards )
  + FERPA (Educational Privacy Act)
  + FFIEC (Financial institutions regulation)
  + FIPS (Gov. Security Standards)
  + FISMA (Federal Info-Sec management)
  + GxP (Quality Guidelines & Regulations)
  + HIPAA (Protected Health Info)
  + ITAR (International Arms Regulations)
  + MPAA (Protected media Content)
  + NIST (National Institute of Standards & Tech.)
  + SEC Rule 17a-4(f) (Financial Data Standards) 
  + VPAT / Section 508 (Accessibility Standards)
- APAC + Europe
  + APAC
    * FISC, JP (Financial Industry infor systems)
    * IRAP, AUS (Australia Security Standards)
    * K-ISMS, KOR (Korean Info-Sec)
    * MTCS Tier 3, SING (Multi-Tier Cloud Security Standard)
    * My Number Act, JP (Personal Info protection)
  + Europe  
    * C5, DEU (Operational Security Attestation)
    * Cyber essentials Plus, UK (Cyber threa protection)
    * ENS High, SPAIN (Spanish Gov standards)
    * G-Cloud, UK (UK Gov Standards)
    * IT-Grundschutz, DEU (Baseline Protection Methodology)
- don't have to know these all ^^
- **JUST BECAUSE AWS COMPLIANT, DOESN'T MEAN YOU ARE**
- **GET AUDITED**

### Artifact

- **portal that provides customers on-demand access to AWS compliance documentation & agreements**
- *Artifact reports*: download AWS security & compliance docs 
- *Artifact Agreements*: review, accept, track status of AWS agreements
- can be used to support internal audit/compliance 
